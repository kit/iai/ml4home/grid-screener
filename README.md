# Grid Screener - A Tool for Automated High-throughput Screening on Biochemical and Biological Analysis Platforms
[Marcel P. Schilling](mailto:marcel.schilling@kit.edu), Svenja Schmelzer, Joaquín Urrutia Gómez, Anna A. Popova, Pavel A. Levkin, Jona Stuck, Markus Reischl

![Grid Screener](gui.png)

### Abstract
Grid structures are common in high-throughput assays to parallelize experiments in biochemical or biological experiments. Manual analysis of grid images is laborious, time-consuming, expensive, and critical in terms of reproducibility. However, it is still common to do such analysis manually, as there is no standardized software for automated analysis. 
In this paper, we introduce a generic method to automatically detect grid structures in images and to perform flexible spot-wise analysis after successful grid detection.
The deep learning-based approach of the grid structure detection allows being flexible concerning different grid types. The combination with a robust parameter estimation algorithm lowers the requirements of the detection quality and thus enhances robustness. 
Further, the method conducts semi-automated grid detection if a fully automated processing fails.
An open-source software tool Grid Screener that implements the proposed methods is provided as a ready-for-use tool for researchers. The usability is demonstrated by taking different criteria into account, which are important for a successful application.
We present the benefits of our proposed tool Grid Screener utilizing three different grid types in the context of high-throughput screening to show our contribution towards further lab automation. Our tool performs much faster than manual analysis, while maintaining or even enhancing accuracy. 

## Citation
BIBTEX
```
@ARTICLE{9650903,
  author={Schilling, Marcel P. and Schmelzer, Svenja and Gómez, Joaquín Eduardo Urrutia and Popova, Anna A. and Levkin, Pavel A. and Reischl, Markus },
  journal={IEEE Access}, 
  title={Grid Screener: A Tool for Automated High-Throughput Screening on Biochemical and Biological Analysis Platforms}, 
  year={2021},
  volume={9},
  number={},
  pages={166027-166038},
  doi={10.1109/ACCESS.2021.3135709}}
```

M. P. Schilling, S. Schmelzer, J. E. U. Gómez, A. A. Popova, P. A. Levkin and M. Reischl, "Grid Screener: A Tool for Automated High-Throughput Screening on Biochemical and Biological Analysis Platforms," in IEEE Access, vol. 9, pp. 166027-166038, 2021, doi: 10.1109/ACCESS.2021.3135709.

## Installation
Clone gitlab repository via 
`git clone https://git.scc.kit.edu/sc1357/grid-screener.git`

Install pip package via 
`pip install -e PATH_TO_YOUR_CLONED_REPO`

## Usage
### Start Grid Screener
`python run_grid_screener.py`

### Implement a custom spot-wise processing function
Add custom spot-wise processing class in `processing_fcn/YOUR_FUNCTION_SCRIPT.py` including processing method `run_process(self,bf_img,params,fl_img=None)` and corresponding result export `export_results(self, processing_dir,results, bf_img,params,fl_img=None)`.

Example
```
class YourProcessingFunction(ProcessingFcn):
    def __init__(self, **kwargs):
        """ initialize your processing function here

        Keyword arguments:
        kwargs -- keyworded arguments
        """

        super(YourProcessingFunction,self).__init__()
        

    def run_process(self,bf_img,params,fl_img=None):
        """ do processing here

        Keyword arguments:
        bf_img (np.array) -- bright-field image
        params (GridParameter) -- grid parameter object
        fl_img (np.array or NoneType) -- flourescent image (default=None)
        """

        # get grid parameters using GridParameter object
        alpha   = params.get_angle()    #np.float32
        d       = params.get_diameter() #np.float32
        grid    = params.get_grid()     #np.array -> shape NxMx2

        # do processing with selected bf_img or fl_img

        return results

    def export_results(self, processing_dir,results, bf_img,params,fl_img=None):
        """ do result export here

        Keyword arguments:
        processing_dir (str) -- processing directory where results are saved
        results -- returned results object of processing function
        bf_img (np.array) -- bright-field image
        params (GridParameter) -- grid parameter object
        fl_img (np.array or NoneType) -- flourescent image (default=None)
        """

```

An example is depicted in the class `CropSpots(ProcessingFcn)`.

## Build in functions
### Grid detection methods
The first step in image processing with this program is grid detection. Two options are available here for this purpose. The first is an automated variant that works with a neural network. For this you have to load a configuration and weights. The second variant is semi-automatic, where you have to select certain points on the image, with which the grid is calculated. Further information for both options are steted below.<br />
After the grid detection was successful, the bf image is displayed, on which the border and the center points are marked. This option can be disabled by unchecking "show grid after detection".

#### Automated grid detection
Choose a tar-file, containing configuration and weights of a pre-trained model.<br />
<br />
1.) Pre-trained DNN weights:<br />
Pre-trained weights for the proposed deep neural network U-Net are available for dataset DMA and wellplate as well under [here](https://osf.io/b3g5t/)<br />
<br />
2.) Configuration: <br />
A configuration is already available in the software package (folder: GS -> config).

##### Estimated Grid
After the estimation process has finished, a dialoge window pops up, showing the image and the overlaying mask. The dialoge window gives the user the opportunity to adjust the resulting mask by changing the parameters diameter, Δx and Δy. If the result is not acceptable at all, that might be due to falsly resized images. The user has two options here:<br />
1.) The user can measure the diameter of one element in the dialoge window by pressing the button "Measured Diameter", closing the window and putting the measured diameter in the corresponding input field. The scale factor will change accordingly and the estimation process can be done again.<br />
2.) The user can input an improved scale factor himself if possible and start the estimation process again.


#### Semi-Automated grid detection
Insert numbers of rows and numbers of columns and chose the shape (available options are square and circle). After starting the grid detection, the inserted picture will appear and 5 clicks have to be made while the Ctrl button is clicked.

![Click examples](click_documentation_1.png)

-First click: left edge of top left spot <br />
-Second click: right edge of top left spot <br />
-Third click: centre of top left spot <br />
-Fourth click: centre of top right spot <br />
-Fifth click: centre of bottom right spot <br />

### Available processes
Before a process is started the grid identification must have been successful. This can be checked by the existence of a file named "grid.params" in the results folder of the bf image. Furthermore, a warning will appear on the interface if no such file exists.<br />
For each process a bf image must be selected. In addition, a fl image must be stored for some processes. Information about the individual processes is shown below. <br />
The results are saved in the folder belonging to the image in the processing folder. When disabeling save results, this step is skipped.<br /> 
<br />
1.) Crop_spots<br />
This process divides the image and creates one image section for each spot in the bf picture.<br />
<br />
2.) Cells_per_spot<br />
This process creates an image section for each spot in the fl picture. Also a list is created with the number of cells that are detected in every spot. A fluroescence picture is necessary to run this process, as well as a bf picture.<br />
<br />
3.) Mean_intensity_fluorescence<br />
The mean intensity of the fluorescence picture is listed for every spot in the grid. Therefore a fl picture as well as a bf picture have to be chosen to be able to run the process. The results are saved as a text document which is structured like the original grid array with rows and columns.
<br />
<br />
4.) Spheroid Analysis<br />
Analysis of spheroid properties.
<br />
<br />
5.) Color Evaluation<br />
Analysis of the intensity of a color for every spot.
The calculation parameters to find the color saturation have to be adjusted manually first. There are two methods which can be chosen: the HSV variant, which calculates the chanels hue, saturation and value. The desired chanel has to be chosen manually. <br />
The RGB method calculates the saturation of different color components. Therefore the color components and the reference color have to be adjusted manually first, whereas the default colors for the saturation are pink and blue and for the reference color white. <br />
The results are then saved as a csv document.

![color_evaluation](color_evaluation.png) 
<br />
<br />
6.) Build Annotated Dataset<br />
In order to be able to train a model for the automatic detection feature later, an annotated data set is created. 
For that, the inserted image and calculated mask (semi-automated grid image in grayscale) are cropped into square pieces. 
The user can specify the width/height of the crops and also the ratio between test and training dataset size. <br />
The program outputs two folders, containing labeled and unlabeled data for test and training and a json-file containing additional data.
Moreover, four empty folders and a "kaida_project.json"-file are created, which thus form a complete kaida-project directory. 
The project can then be loaded inside the kaida environment in order to succesfully train a model there for automated segmentation.
The resulting tar-file can then again be used for automated grid detection inside the grid-screener environment, as described earlier.<br />

Detailed descriptions of how the kaida-training works, can be found [here](https://git.scc.kit.edu/sc1357/kaida#kaida-karlsruhe-image-data-annotation-tool)



## Application Video
Available under [here](https://osf.io/b3g5t/)
