from setuptools import setup, find_packages

setup(
    name="Grid Screener",
    version="0.1.0",
    author="Marcel Schilling, Svenja Schmelzer",
    author_email="marcel.schilling@kit.edu",
    url="https://git.scc.kit.edu/sc1357/grid-screener",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    packages=find_packages(),
    install_requires=[],
)