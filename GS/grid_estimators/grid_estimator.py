from abc import ABC, abstractmethod

class GridEstimator(ABC):
    def __init__(self):
        super().__init__()

    @abstractmethod
    def process(self, mask):
        raise NotImplementedError("Process fcn needs to be implemented")