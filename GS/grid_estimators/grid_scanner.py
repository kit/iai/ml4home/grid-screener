from numpy.lib.function_base import median
import cmath
import copy

import cv2
import numpy as np
from numpy.lib.function_base import median
from skimage.measure import label, regionprops
from skimage.transform import rotate
from sklearn.cluster import DBSCAN
from sklearn.linear_model import RANSACRegressor
from sklearn.neighbors import NearestNeighbors

from GS.grid_estimators.manual_detector import ManualDetector
from GS.grid_estimators.grid_estimator import GridEstimator


class Scanner(GridEstimator):
    def __init__(
        self,
    ):
        super(Scanner,self).__init__()

    def process(self, mask,resize_factor):
        """
        Process an input mask to estimate rotation, grid parameters, shape classification, and size.

        Args:
            self (object): The instance of the class.
            mask (numpy.ndarray): The input mask as a NumPy array.
            resize_factor (float): The factor by which to resize the mask.

        Returns:
            Tuple[float, numpy.ndarray, str, float]: A tuple containing the estimated rotation angle
            in degrees, grid parameters as a NumPy array, shape classification as a string, and size as a float.

        Note:
            This method processes an input mask to estimate various parameters. It performs operations
            such as resizing, smoothing, label extraction, area filtering, centroid extraction, rotation
            angle estimation, rotation correction, clustering, line fitting, and intersection calculation.
            The result is a tuple containing the estimated rotation angle, grid parameters, shape classification,
            and size.
        """

        self.resize_factor=resize_factor                
        reg_type = "ransac"

        ori_mask = copy.deepcopy(mask)
        
        if self.resize_factor < 1:
            mask=self._resize(mask)

        # pre-processing to smooth mask
        mask = self.do_opening(mask)
        mask = self.do_closing(mask)
        
        labels  = label(mask)
        r_props = regionprops(labels)

        # area-based filtering
        labels = self.area_filter(labels,r_props)
    
        # extract centroids
        r_props = regionprops(labels)
        points = self.extract_centroid(r_props)
        
        # estimate rotation angle
        rot_angle, dist = self.estimate_rotation_angle(points)
        if np.isnan(rot_angle):
            rot_angle, dist = self.estimate_rotation_angle(points, n_neighbors=4)
        if np.isnan(rot_angle):
            rot_angle, dist = self.estimate_rotation_angle(points, n_neighbors=3)

        # rotation correction
        try:
            mask = ManualDetector.rotate(self,image=mask,angle=np.rad2deg(rot_angle)).astype("int") 
        except cv2.error:
            mask = rotate(mask,np.rad2deg(rot_angle),resize=True, preserve_range=True, order=0).astype("int")

        labels  = label(mask)
        labels  = label(mask)
        r_props = regionprops(labels)

        labels = self.area_filter(labels,r_props)

        # extract centroids
        r_props = regionprops(labels)
        points = self.extract_centroid(r_props)

        # clustering in horizontal/verical direction
        w, h = mask.shape
        if w > h:
            hor_min_samples = 2
            ver_min_samples = 4
        elif w < h:
            hor_min_samples = 4
            ver_min_samples = 2
        else:
            hor_min_samples = 2
            ver_min_samples = 2

        clustering_hor = DBSCAN(eps=dist*0.4, min_samples=hor_min_samples).fit(np.array(points[:,1]).reshape(-1,1))
        clustering_ver = DBSCAN(eps=dist*0.4, min_samples=ver_min_samples).fit(np.array(points[:,0]).reshape(-1,1))

        # sorting cluster results
        ver_labels = np.unique(clustering_ver.labels_)
        ver_labels = np.delete(ver_labels, np.where(ver_labels == -1))
        cluster_center_x = list()
        for segment in ver_labels:
            index = clustering_ver.labels_==segment
            cluster_center_x.append(np.mean(points[index,0]))
        ver_labels = ver_labels[np.argsort(np.array(cluster_center_x))]

        hor_labels = np.unique(clustering_hor.labels_)
        hor_labels = np.delete(hor_labels, np.where(hor_labels == -1))
        cluster_center_y = list()
        for segment in hor_labels:
            index = clustering_hor.labels_==segment
            cluster_center_y.append(np.mean(points[index,1]))
        hor_labels = hor_labels[np.argsort(np.array(cluster_center_y))]

        # obtaining different grid params
        num_rows        = len(hor_labels)
        num_cols        = len(ver_labels)

        if num_rows==0 and hor_min_samples==4:
            hor_min_samples=2
            clustering_hor = DBSCAN(eps=dist*0.4, min_samples=hor_min_samples).fit(np.array(points[:,1]).reshape(-1,1))
            hor_labels = np.unique(clustering_hor.labels_)
            hor_labels = np.delete(hor_labels, np.where(hor_labels == -1))
            cluster_center_y = list()
            for segment in hor_labels:
                index = clustering_hor.labels_==segment
                cluster_center_y.append(np.mean(points[index,1]))
            hor_labels = hor_labels[np.argsort(np.array(cluster_center_y))]
            num_rows        = len(hor_labels)
        elif num_cols==0 and ver_min_samples==4:
            ver_min_samples = 2
            clustering_ver = DBSCAN(eps=dist*0.4, min_samples=ver_min_samples).fit(np.array(points[:,0]).reshape(-1,1))
            ver_labels = np.unique(clustering_ver.labels_)
            ver_labels = np.delete(ver_labels, np.where(ver_labels == -1))
            cluster_center_x = list()
            for segment in ver_labels:
                index = clustering_ver.labels_==segment
                cluster_center_x.append(np.mean(points[index,0]))
            ver_labels = ver_labels[np.argsort(np.array(cluster_center_x))]

        width_lst = list()
        height_lst = list()
        for region in r_props:
            width_lst.append(region["image"].shape[1])
            height_lst.append(region["image"].shape[0])
        d = np.mean([np.median(height_lst), np.median(width_lst)]) / self.resize_factor

        shape = self.shape_classification(r_props)

        grid_params     = np.zeros((num_rows,num_cols,2))

        if num_cols == 1:
            index=clustering_ver.labels_ == ver_labels[0]
            centroids = points[index]
            grid_params = np.zeros((centroids.shape[0],num_cols,2))
            for i_r in range(centroids.shape[0]):
                for i_c in range(num_cols):
                    grid_params[i_r,i_c,:]=centroids[i_r][0],centroids[i_r][1]
        elif num_rows == 1:
            index=clustering_hor.labels_ == hor_labels[0]
            centroids = points[index]
            grid_params = np.zeros((num_rows,centroids.shape[1],2))
            for i_r in range(num_rows):
                for i_c in range(centroids.shape[1]):
                    grid_params[i_r,i_c,:]=centroids[i_r][0],centroids[i_r][1]
        else:
            # line fit
            ver_lines_params = list()
            for segment_ver in ver_labels:
                index = clustering_ver.labels_==segment_ver
                f = self.line_predictor(points[index,1], points[index,0], type=reg_type)
                p_1 = [f(0),0]
                p_2 = [f(mask.shape[0]), mask.shape[0]]

                A = np.array([[p_1[0], p_1[1]],
                            [p_2[0], p_2[1]]])
                b = np.array([[1], [1]])

                n_0_v, n_1_v = np.linalg.solve(A, b)
                ver_lines_params.append([n_0_v[0], n_1_v[0]])

            hor_lines_params = list()
            for segment in hor_labels:
                index = clustering_hor.labels_==segment
                f = self.line_predictor(points[index,0], points[index,1], type=reg_type)

                p_1 = [0, f(0)]
                p_2 = [mask.shape[0],f(mask.shape[1])]

                A = np.array([[p_1[0], p_1[1]],
                            [p_2[0], p_2[1]]])
                b = np.array([[1], [1]])

                n_0_h, n_1_h = np.linalg.solve(A, b)
                hor_lines_params.append([n_0_h[0], n_1_h[0]])

       
        # calculate intersections
        for i_r in range(num_rows):
            for i_c in range(num_cols):
                A = np.array([[hor_lines_params[i_r][0], hor_lines_params[i_r][1]],
                            [ver_lines_params[i_c][0], ver_lines_params[i_c][1]]])

                b = np.array([[1], [1]])
                x, y = np.linalg.solve(A, b)

                grid_params[i_r,i_c,:] = x[0] / self.resize_factor, y[0] /self.resize_factor
     
        return (np.rad2deg(rot_angle), grid_params, shape, d)

    def delete_rows(self,mask):
        """
        Delete specific rows of zeros from the input mask.

        Args:
            self (object): The instance of the class.
            mask (numpy.ndarray): The input mask as a NumPy array.

        Returns:
            numpy.ndarray: The modified mask with specific rows set to zero.

        Note:
            This method takes an input mask as a NumPy array and sets specific rows to zero.
            The rows to be deleted are defined by slicing the mask using specific column indices.
        """

        mask[:,1003:1381]=0
        mask[:,2361:2661]=0
        mask[:,316:619]=0
        mask[:,3123:3346]=0
        mask[:,1679:1910]=0
        return mask

    def estimate_rotation_angle(
        self,
        points, 
        tolerance_angle=0.1, 
        tolerance_r=0.1,
        n_neighbors = 5):
        """
        Estimate the rotation angle and distance from a set of points.

        Args:
            self (object): The instance of the class.
            points (numpy.ndarray): An array of points as complex numbers (x + yi).
            tolerance_angle (float, optional): Tolerance for angle comparison. Defaults to 0.1.
            tolerance_r (float, optional): Tolerance for radius comparison. Defaults to 0.1.
            n_neighbors (int, optional): Number of nearest neighbors to consider. Defaults to 5.

        Returns:
            Tuple[float, float]: A tuple containing the estimated rotation angle in radians and
            the median distance from the points to their nearest neighbors.

        Note:
            This method estimates the rotation angle and distance based on a set of points in a
            complex number format. It calculates the angle and radius between each point and its
            nearest neighbors, considering a specified number of neighbors (n_neighbors).
            The method applies plausibility checks on angle and radius values and returns the median
            rotation angle and median distance from the points to their nearest neighbors.
        """

        rot_angle_lst   = list()
        dist_lst        = list()
        if n_neighbors == 3:
            ref_angle_1       = [-np.pi, 0]
            ref_angle_2       = [-np.pi/2, np.pi/2]
        elif n_neighbors == 4:
            ref_angle_1       = [-np.pi/2, 0, np.pi/2]
            ref_angle_2       = [-np.pi, 0, np.pi/2]
            ref_angle_3       = [-np.pi, -np.pi/2, np.pi/2]
            ref_angle_4       = [-np.pi, -np.pi/2, 0]
        elif n_neighbors == 5:
            ref_angle       = [-np.pi, -np.pi/2, 0, np.pi/2]

        nbrs = NearestNeighbors(n_neighbors=n_neighbors, algorithm='ball_tree').fit(points)
        
        for point in points:
            indices     = nbrs.kneighbors(np.array([point]), return_distance=False)
            angle_lst   = list()
            r_lst       = list()
            for i in range(1,n_neighbors):
                ref_point = points[indices[0,i],:]
                c_point = complex(ref_point[0]-point[0],ref_point[1]-point[1])
                angle_lst.append(cmath.phase(c_point))
                r_lst.append(abs(c_point))

            angle_lst = np.array(angle_lst)
            r_lst = np.array(r_lst)

            sort_index = np.argsort(angle_lst)
            angle_lst = angle_lst[sort_index]
            r_lst = r_lst[sort_index]

            if n_neighbors == 3:
                angle_plausibilty_check = np.logical_and(
                    np.diff(angle_lst) >= (1-tolerance_angle)*np.pi, 
                    np.diff(angle_lst) <= (1+tolerance_angle)*np.pi)
            elif n_neighbors == 4:
                angle_plausibilty_check = np.logical_and(
                    np.diff(angle_lst) >= (1-tolerance_angle)*np.pi/2, 
                    np.diff(angle_lst) <= (1+tolerance_angle)*np.pi)
            elif n_neighbors == 5:
                angle_plausibilty_check = np.logical_and(
                    np.diff(angle_lst) >= (1-tolerance_angle)*np.pi/2, 
                    np.diff(angle_lst) <= (1+tolerance_angle)*np.pi/2)

            r_plausibilty_check = np.logical_and(
                r_lst >= (1-tolerance_r)*np.mean(r_lst), 
                r_lst <= (1+tolerance_r)*np.mean(r_lst)
            )

            if angle_plausibilty_check.all() and r_plausibilty_check.all():
                if n_neighbors == 3:
                    mean_angle_1 = np.mean(angle_lst-ref_angle_1)
                    mean_angle_2 = np.mean(angle_lst-ref_angle_2)
                    mean_angle = min(np.abs([mean_angle_1,mean_angle_2]),key=abs)
                elif n_neighbors == 4:
                    mean_angle_1 = np.mean(angle_lst-ref_angle_1)
                    mean_angle_2 = np.mean(angle_lst-ref_angle_2)
                    mean_angle_3 = np.mean(angle_lst-ref_angle_3)
                    mean_angle_4 = np.mean(angle_lst-ref_angle_4)
                    mean_angle = min([mean_angle_1,mean_angle_2,mean_angle_3,mean_angle_4],key=abs)
                elif n_neighbors == 5:
                    mean_angle = np.mean(angle_lst-ref_angle)
                rot_angle_lst.append(mean_angle if mean_angle< np.pi/4 else -(np.pi/2-mean_angle))
                dist_lst.append(np.mean(r_lst))
            
        return np.median(rot_angle_lst), np.median(dist_lst)


    def area_filter(self,labels, r_props, q_delta=0.05):
        area_lst = list()
        for region in r_props:
            area_lst.append(region["area"])
        labels_to_remove    = list()

        for region in r_props:
            if region["area"] < 0.75*median(area_lst) or region["area"]>1.25*median(area_lst):
                labels_to_remove.append(region["label"])

        """# MDA
        for region in r_props:
            x=region["area"]
            if (abs(x - median(area_lst)) / median_abs_deviation(area_lst)) > 3:
                labels_to_remove.append(region["label"])"""

        """# IQR
        q75, q25 = np.percentile(area_lst, [75 ,25])
        iqr = q75 - q25

        for region in r_props:
            if region["area"] < q25-1.5*iqr or region["area"] > q75+(1.5*iqr):
                labels_to_remove.append(region["label"])"""

        """# Quantiles
        for region in r_props:
            if not (np.quantile(area_lst, q_delta)<=region["area"]<=np.quantile(area_lst, 1-(2*q_delta))):
                labels_to_remove.append(region["label"])"""

        labels[np.isin(labels, labels_to_remove)] = 0
        return labels


    def extract_centroid(self,r_props):
        x_lst = list()
        y_lst = list()
        for region in r_props:
            x_lst.append(region["centroid"][1])
            y_lst.append(region["centroid"][0])

        points = np.array([x_lst,y_lst]).transpose()
        return points

    def do_opening(self,mask, kernel_size=3, iterations = 1):
        kernel= np.ones((kernel_size, kernel_size), np.uint8)
        return cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel, iterations=iterations)


    def do_closing(self,mask, kernel_size=3, iterations=1):
        kernel = np.ones((kernel_size,kernel_size),np.uint8)
        return cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel, iterations=iterations)


    def shape_classification(self,r_props):
        shape_lst = list()
        for region in r_props:
            d = np.mean([region["image"].shape[0],region["image"].shape[1]])
            area_circle = np.pi/4*d**2
            area_box =  region["image"].shape[0] *region["image"].shape[1]
            if np.abs(region["area"]-area_circle)<np.abs(region["area"]-area_box):
                shape_lst.append("circle")
            else:
                shape_lst.append("square")

        return max(set(shape_lst), key = shape_lst.count) 

    def line_predictor(self,x_data, y_data, type="normal"):
        if type == "normal":
            a = np.polyfit(x_data, y_data, 1)
            f = np.poly1d(a)
            return f
        elif type == "ransac":
            reg = RANSACRegressor(random_state=0).fit(x_data.reshape(-1,1),y_data)
            return lambda x: reg.predict(np.array([x]).reshape(-1,1))[0]

    def _resize(self, img):
        scale_percent = self.resize_factor * 100
        width = int(img.shape[1] * scale_percent / 100)
        height = int(img.shape[0] * scale_percent / 100)
        dim = (width, height)
        img = cv2.resize(img, dim)
        return img