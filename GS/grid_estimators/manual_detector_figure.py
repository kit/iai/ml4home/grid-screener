from PyQt5 import QtCore, QtGui, QtWidgets, uic
import logging

import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvas, FigureCanvasQTAgg, NavigationToolbar2QT
from matplotlib.figure import Figure

import numpy as np

class MPLWidget(QtWidgets.QWidget):
    """
    Widget which should act like a matplotlib FigureCanvas, but embedded in PyQt5
    """

    def __init__(self,img, dialog, max_click, parent=None):
        super().__init__(parent)
        self.dialog=dialog
        self.max_click=max_click
        #self.main_frame=QtWidgets.QWidget()
        self.canvas = FigureCanvas(Figure())

        self.pbar = QtWidgets.QProgressBar(self)
        self.pbar.setFixedWidth(300)
        self.progresses=[0,20,40,60,80]
        
        #self.initUI()
        self.canvas.setParent(self)
        #self.canvas.setFocusPolicy( QtCore.Qt.ClickFocus )
        #self.canvas.setFocus()
        ax = self.canvas.figure.add_subplot(111)
        self.ax_titles=["click at LEFT edge of spot 1 (upper left corner)","click at RIGHT edge of spot 1 (upper left corner)", "click in the CENTER of spot 1 (upper left corner)", "click in the CENTER of spot 2 (upper right corner)", "click in the CENTER of spot 3 (lower right corner)"]
        ax.imshow(img,cmap="gray")
        
        self.cid_enter = self.canvas.mpl_connect('axes_enter_event', self.on_enter_event)

        self.callbacks = list()

        self.cursor_obj = Cursor(ax)
        self.callbacks.append(self.canvas.mpl_connect("motion_notify_event", self.cursor_obj.on_mouse_move))

        self.key_listener = KeyListener()
        self.callbacks.append(self.canvas.mpl_connect("key_press_event", self.key_listener.on_click))
        self.callbacks.append(self.canvas.mpl_connect("key_release_event", self.key_listener.on_release))

        self.click_collector = ClickCollector(self,self.max_click)
        self.callbacks.append(self.canvas.mpl_connect("button_press_event", self.click_collector.on_click))

        self.layout = QtWidgets.QVBoxLayout(self)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.toolbar = NavigationToolbar2QT(self.canvas, self)

        self.delete_button=QtWidgets.QPushButton("Delete last click",self)
        self.delete_button.setToolTip("Delete the last clicked coordinates")
        self.delete_button.setIcon(self.style().standardIcon(getattr(QtWidgets.QStyle, "SP_ArrowLeft")))
        self.delete_button.clicked.connect(self.click_collector.delete_click)

        self.close_button=QtWidgets.QPushButton("Cancel",self)
        self.close_button.setToolTip("Cancel coordinate selection and return to grid-screener interface")
        self.close_button.setIcon(self.style().standardIcon(getattr(QtWidgets.QStyle, "SP_BrowserStop")))
        self.close_button.clicked.connect(self.cancel)

        self.label=QtWidgets.QLabel(self)
        self.label.setFont(QtGui.QFont("Times Roman",14))
        self.label.setText("Hold Crtl-key and " + '\n' + self.ax_titles[0])
        self.label.setAlignment(QtCore.Qt.AlignTop)

        self.horizontalLayout = QtWidgets.QHBoxLayout(self)
        self.horizontalLayout.addWidget(self.toolbar)
        self.horizontalLayout.addWidget(self.pbar)
        self.horizontalLayout.addWidget(self.label)
        self.horizontalLayout.addStretch()
        self.horizontalLayout.addWidget(self.delete_button)
        self.horizontalLayout.addWidget(self.close_button)
        self.layout.addLayout(self.horizontalLayout)
        self.layout.addWidget(self.canvas)
    

    def update_title(self):
        counter=len(self.click_collector.coordinates)
        self.label.setText("Hold Crtl-key and " + '\n' + self.ax_titles[counter])
        self.pbar.setValue(self.progresses[counter])

    def on_enter_event(self, _):
        self.canvas.setFocus()

    def close(self):
        for callback in self.callbacks:
            self.canvas.mpl_disconnect(callback)

        self.dialog.close()

    def cancel(self):
        choice = QtWidgets.QMessageBox.question(self, 'Message',
                                     "Are you sure you want to cancel?", QtWidgets.QMessageBox.Yes |
                                     QtWidgets.QMessageBox.No, QtWidgets.QMessageBox.No)
        if choice == QtWidgets.QMessageBox.Yes:
            # necessary step to pass while loop in ManualAnnotaionsFigure
            self.max_click=len(self.click_collector.coordinates)
            self.dialog.close()
        else:
            pass
        

    def key_pressed(self) -> bool:
        return self.key_listener.is_key_pressed(" ") or self.key_listener.is_key_pressed("control")
        
class PlotWindow(QtWidgets.QDialog):
    """Window which is a stand in for the larger PyQt5 application which needs a plot embedded"""

    def __init__(self,img,max_click):
        super().__init__()
        self.showFullScreen()
        self.setWindowTitle("Manual Detector")

        self.mplwidget = MPLWidget(img,self,max_click)
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.mplwidget)
        self.setLayout(layout)
        

class ManualAnnotationsFigure:
    def __init__(self, img, num_clicks):
        self.fig = None
        self.callbacks = list()
        self.key_listener = None
        self.click_collector = None

        self.max_click = num_clicks

        self.plot_win=PlotWindow(img,self.max_click)
        self.plot_win.exec_()


        while len(self.get_coordinates()) != self.max_click:
            self.plot_win.mplwidget.close()
            logging.info(f"Not enough points were given {len(self.get_coordinates())} of {self.max_click}. Restarting UI...".format(len(self.get_coordinates())))
            self.plot_win=PlotWindow(img)
            self.plot_win.exec_()
            #self.init_ui(img)
            

    def get_coordinates(self):
        return self.plot_win.mplwidget.click_collector.coordinates
    

class ClickCollector:
    def __init__(self, annotations_figure: ManualAnnotationsFigure, max_click):
        self.coordinates = list()

        self.annotations_figure = annotations_figure

        self.max_click = max_click

    def _is_valid(self, x, y) -> bool:
        if x < 0 or y < 0:
            return False

        for c_x, c_y in self.coordinates:
            if int(x) == int(c_x) and int(y) == int(c_y):
                return False

        return True

    def on_click(self, event):
        if not self.annotations_figure.key_pressed():
            return

        x, y = event.xdata, event.ydata

        if not self._is_valid(x, y):
            logging.info("Ignored invalid click ({}, {})".format(x, y))
            return

        logging.info("Registered click ({}, {})".format(x, y))
        
        self.coordinates.append((x, y))


        if len(self.coordinates) >= self.max_click:
            self.annotations_figure.close()
        else:
            self.annotations_figure.update_title()


    def delete_click(self):
            self.coordinates=self.coordinates[:-1]
            self.annotations_figure.update_title()
            


class KeyListener:
    def __init__(self):
        self.keys_pressed = dict()

    def on_click(self, event):
        self.keys_pressed[event.key] = True

    def on_release(self, event):
        self.keys_pressed[event.key] = False

    def is_key_pressed(self, key: str) -> bool:
        if key not in self.keys_pressed:
            return False
        return self.keys_pressed[key]


class Cursor:
    """
    A cross hair cursor.
    """
    def __init__(self, ax):
        self.ax = ax
        self.horizontal_line = ax.axhline(color='r', lw=0.8, ls='--')
        self.vertical_line = ax.axvline(color='r', lw=0.8, ls='--')
        # text location in axes coordinates
        self.text = ax.text(0.72, 0.9, '', transform=ax.transAxes)

    def set_cross_hair_visible(self, visible):
        need_redraw = self.horizontal_line.get_visible() != visible
        self.horizontal_line.set_visible(visible)
        self.vertical_line.set_visible(visible)
        self.text.set_visible(visible)
        return need_redraw

    def on_mouse_move(self, event):
        if not event.inaxes:
            need_redraw = self.set_cross_hair_visible(False)
            if need_redraw:
                self.ax.figure.canvas.draw()
        else:
            self.set_cross_hair_visible(True)
            x, y = event.xdata, event.ydata
            # update the line positions
            self.horizontal_line.set_ydata(y)
            self.vertical_line.set_xdata(x)
            self.text.set_text('x=%1.2f, y=%1.2f' % (x, y))
            self.ax.figure.canvas.draw()
