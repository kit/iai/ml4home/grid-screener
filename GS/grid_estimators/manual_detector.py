import logging
import os

import cv2
import numpy as np
from scipy import ndimage
from PyQt5 import QtCore, QtGui, QtWidgets, uic
from GS.grid_estimators.grid_estimator import GridEstimator
from GS.grid_estimators.manual_detector_figure import ManualAnnotationsFigure
from GS.utils.image import Image

import time


class ManualDetector(GridEstimator):
    def __init__(self, save_user_annotations: bool = False):
        super(ManualDetector, self).__init__()
        self.resize_factor = 0.2
        self.save_user_annotations = save_user_annotations
    
    def _resize(self, img):
        """
        Resize the input image.

        Args:
            self (object): The instance of the class.
            img (numpy.ndarray): The input image as a NumPy array.

        Returns:
            numpy.ndarray: The resized image as a NumPy array.

        Note:
            This method resizes the input image by a specified resize factor.
        """

        logging.info(f"Downsample image by factor {self.resize_factor} in order to boost calculation")
        scale_percent = self.resize_factor * 100
        width = int(img.shape[1] * scale_percent / 100)
        height = int(img.shape[0] * scale_percent / 100)
        dim = (width, height)
        img = cv2.resize(img, dim)
        return img

    def _get_points(self, img: np.ndarray, num_groups, base_image):
        """
        Get user-defined points on the image.

        Args:
            self (object): The instance of the class.
            img (numpy.ndarray): The input image as a NumPy array.
            num_groups (int): The number of point groups to obtain.
            base_image (Image): The base image for saving user annotations.

        Returns:
            List[Tuple[float, float]]: A list of user-defined points.

        Note:
            This method allows users to click on the image to define points.
            If `save_user_annotations` is set to True, the annotations will be saved.
        """

        clicks = ManualAnnotationsFigure(img, num_groups).get_coordinates()
        if self.save_user_annotations:
            self._save_user_annotations(clicks, image=base_image)

        return clicks

    def _calculate_grid(self, img, img_orig, points, columns, rows):
        """
        Calculate the grid parameters based on user-defined points.

        Args:
            self (object): The instance of the class.
            img (numpy.ndarray): The input image as a NumPy array.
            img_orig (numpy.ndarray): The original image as a NumPy array.
            points (List[Tuple[float, float]]): User-defined points.
            columns (int): The number of columns in the grid.
            rows (int): The number of rows in the grid.

        Returns:
            Tuple[numpy.ndarray, float]: A tuple containing the grid parameters as a NumPy array
            and the estimated rotation angle in degrees.
        """

        if columns >1 and  rows >1:
            d_x_hor = points[1][0] - points[0][0]
            d_y_hor = points[1][1] - points[0][1]

            d_x_ver = points[2][0] - points[1][0]
            d_y_ver = points[2][1] - points[1][1]

            angle_hor = np.arctan(d_y_hor / d_x_hor)
            angle_ver = np.arctan(-d_x_ver / d_y_ver)
            
            denum = rows + columns
            angle = np.rad2deg(np.average([angle_hor, angle_ver],
                                weights=[columns / denum, rows / denum]))

        elif columns ==1:
            d_x_ver = points[1][0] - points[0][0]
            d_y_ver = points[1][1] - points[0][1]
            angle = np.rad2deg(np.arctan(-d_x_ver / d_y_ver))
        else:
            d_x_hor = points[1][0] - points[0][0]
            d_y_hor = points[1][1] - points[0][1]
            angle = np.rad2deg(np.arctan(d_y_hor / d_x_hor))        

        point = [points[0][0] / self.resize_factor, points[0][1] / self.resize_factor]

        if self.resize_factor < 1:
            new = self.rot(img_orig, point, angle)
        else:
            new = self.rot(img, point, angle)

        grid = np.zeros((rows, columns, 3))
        for i_r in range(rows):
            for i_c in range(columns):
                x_i = new[0] + i_c * self.d_pattern
                y_i = new[1] + i_r * self.d_pattern
                d = self.size
                grid[i_r, i_c, :] = x_i, y_i, d

        return grid, angle

    def _save_user_annotations(self, points, image):
        """
        Save user-defined annotations to a CSV file.

        Args:
            self (object): The instance of the class.
            points (List[Tuple[float, float]]): User-defined points.
            image (Image): The base image for saving annotations.
        """

        f = open(os.path.join(image.get_directory(), "{}_user_annotations.csv".format(image.get_name())), "w")
        f.write("\n".join(";".join(str(x) for x in point) for point in points))
        f.close()

    def pythagoras(self, p_0, p_1):
        return ((p_0[0] - p_1[0]) ** 2 + (p_0[1] - p_1[1]) ** 2) ** 0.5

    def compute_d_pattern(self, point_0, point_1, point_2, resize_factor, columns, rows):
        """
        Compute the pattern distance based on user-defined points.

        Args:
            self (object): The instance of the class.
            point_0 (Tuple[float, float]): The first point as a tuple (x, y).
            point_1 (Tuple[float, float]): The second point as a tuple (x, y).
            point_2 (Tuple[float, float], optional): The third point as a tuple (x, y). Defaults to None.
            resize_factor (float): The resize factor.
            columns (int): The number of columns in the grid.
            rows (int): The number of rows in the grid.
        """

        if rows == 1:
            array_size = self.pythagoras(point_0, point_1)/resize_factor
            self.d_pattern = array_size/(columns - 1)
        elif columns ==1:
            array_size = self.pythagoras(point_0, point_1)/resize_factor
            self.d_pattern = array_size/(rows - 1)
        else:
            array_size_x = self.pythagoras(point_0, point_1)/resize_factor
            array_size_y = self.pythagoras(point_1, point_2)/resize_factor

            d_pattern_x = array_size_x/(columns - 1)
            d_pattern_y = array_size_y/(rows - 1)
            self.d_pattern = (d_pattern_x + d_pattern_y)/2

    def rotate(self,image,angle):
        """
        Rotate the input image by a specified angle using cv2.

        Args:
            self (object): The instance of the class.
            image (numpy.ndarray): The input image as a NumPy array.
            angle (float): The rotation angle in degrees.

        Returns:
            numpy.ndarray: The rotated image as a NumPy array.
        """

        image_center = tuple(np.array(image.shape[1::-1]) / 2)
        rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
        result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_NEAREST)
        return result
  
    def rot(self, image, xy, angle):
        """
        Rotate and translate the input image.

        Args:
            self (object): The instance of the class.
            image (numpy.ndarray): The input image as a NumPy array.
            xy (List[float, float]): The translation point as a list [x, y].
            angle (float): The rotation angle in degrees.

        Returns:
            numpy.ndarray: The rotated and translated image as a NumPy array.

        Note:
            This method uses cv2 for rotation unless it leads to an error. Then ndimage is used.

        """
        
        try:
            im_rot=self.rotate(image,angle)
        except cv2.error:
            QtWidgets.QMessageBox.warning(None, 'Warning', 'The image is quite big. Grid estimation might take a while.\nPlease confirm!')
            im_rot = ndimage.rotate(image, angle, reshape=True)

        org_center = (np.array(image.shape[:2][::-1])-1)/2.
        rot_center = (np.array(im_rot.shape[:2][::-1])-1)/2.
        org = xy-org_center
        a = np.deg2rad(angle)
        new = np.array([org[0]*np.cos(a) + org[1]*np.sin(a), -org[0]*np.sin(a) + org[1]*np.cos(a)])
        return new+rot_center
    

    def process(self, image,num_groups,grid_shape):
        """
        Process an input image to estimate grid parameters.

        Args:
            self (object): The instance of the class.
            image (numpy.ndarray): The input image as a NumPy array.
            num_groups (int): The number of point groups.
            grid_shape (List[Tuple[int, int]]): A list of tuples specifying grid shapes.

        Returns:
            Tuple[float, numpy.ndarray, float]: A tuple containing the estimated rotation angle
            in degrees, grid parameters as a NumPy array, and the estimated size.
        """

        image=Image(image)
        img = image.numpy().copy()
        img_orig = img.copy()

        num_clicks = 2
        for nr in range(0,num_groups):
            rows,columns = grid_shape[nr]
            if rows == 1 or columns == 1:
                num_clicks +=2
            else:
                num_clicks += 3


        height, width=img.shape[:2]
        max_f=max(height, width)
        if max_f<8000:
            self.resize_factor=1
        if self.resize_factor < 1:
            img = self._resize(img)

        clicks = self._get_points(img, num_clicks, base_image=image)
        self.size = self.pythagoras(clicks[0],clicks[1])/self.resize_factor

        cl_pos=2
        for nr in range(0,num_groups):
            rows, columns = grid_shape[nr]
            if rows == 1 or columns == 1:
                self.compute_d_pattern(clicks[cl_pos], clicks[cl_pos+1], None, self.resize_factor, columns,rows)
                temp_grid, angle = self._calculate_grid(img, img_orig, clicks[cl_pos:cl_pos+2], columns, rows)
                cl_pos = +2
            else:
                self.compute_d_pattern(clicks[cl_pos], clicks[cl_pos+1], clicks[cl_pos+2], self.resize_factor, columns,rows)
                temp_grid, angle = self._calculate_grid(img, img_orig, clicks[cl_pos:cl_pos+3], columns, rows)
                cl_pos =+ 3

            try:
                grid = np.concatenate((grid, temp_grid))
            except:
                grid = temp_grid

        return angle, grid, self.size
      