from re import I
from PyQt5 import QtCore, QtGui, QtWidgets, uic
import os
import matplotlib.pyplot as plt
import matplotlib.widgets as wdg
import pickle
import copy
import numpy as np
import math
from skimage.draw import disk, rectangle
from PyQt5 import QtCore, QtGui, QtWidgets, uic
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT
from matplotlib.figure import Figure

from GS.utils.grid_parameter import GridParameter

class EstimatedGridDialog(QtWidgets.QDialog):
    """
     Dialog for displaying and interacting with an estimated grid on an image.

    Args:
        angle (float): The angle of the grid.
        d (float): The diameter size of grid elements.
        grid (list of lists): The grid points represented as a list of (x, y) coordinates.
        shape (str): The shape of grid elements, either "square" or "circle".
        bf_img (numpy.ndarray): The brightfield image.
        detection_dir (str): The directory for detection-related files.
        result_dir (str): The directory for result-related files.
        fl_img (numpy.ndarray, optional): The fluorescence image. Default is None.
        detection_fl_dir (str, optional): The directory for fluorescence detection-related files. Default is None.
    """

    def __init__(self,angle,d,grid,shape,bf_img, detection_dir,result_dir, fl_img=None, detection_fl_dir=None):
        super().__init__()

        #Initialize parameters
        self.angle=angle
        self.grid=grid
        self.d=d
        self.shape=shape
        self.org_d=copy.deepcopy(d)
        self.org_grid=copy.deepcopy(grid)
        
        self.bf_img=bf_img
        self.fl_img=fl_img

        self.result_dir=result_dir
        self.detection_dir=detection_dir
        self.detection_fl_dir=detection_fl_dir

        self.linewidth=0.1
        self.markersize=2

        #Initialize important Figures and Axes for all plots
        self.fig_mask=Figure(figsize=(bf_img.shape[1]/1000,bf_img.shape[0]/1000), dpi=100)
        self.ax_mask=self.fig_mask.add_subplot(111)
        self.fig_mask.subplots_adjust(left=0, right=1, bottom=0, top=1)
        self.ax_mask.set_axis_off()
        self.fig_mask.add_axes(self.ax_mask)
        self.ax_mask.imshow(bf_img,cmap="gray")
        self.mask = np.zeros(bf_img.shape[0:2])

        self.fig_border=Figure(figsize=(bf_img.shape[1]/1000,bf_img.shape[0]/1000), dpi=100)
        self.ax_border=self.fig_border.add_subplot(111)
        self.fig_border.subplots_adjust(left=0, right=1, bottom=0, top=1)
        self.ax_border.set_axis_off()
        self.fig_border.add_axes(self.ax_border)
        self.ax_border.imshow(bf_img,cmap="gray")
        self.org_ax_border=copy.deepcopy(self.ax_border)

        if self.fl_img is not None:            
            self.fig_fl_mask=Figure(figsize=(bf_img.shape[1]/1000,bf_img.shape[0]/1000), dpi=100)
            self.ax_fl_mask=self.fig_fl_mask.add_subplot(111)
            self.fig_fl_mask.subplots_adjust(left=0, right=1, bottom=0, top=1)
            self.ax_fl_mask.set_axis_off()
            self.fig_fl_mask.add_axes(self.ax_fl_mask)
            self.ax_fl_mask.imshow(fl_img,cmap="gray")
            self.mask_fl = np.zeros(fl_img.shape[0:2])

            self.fig_fl_border=Figure(figsize=(bf_img.shape[1]/1000,bf_img.shape[0]/1000), dpi=100)
            self.ax_fl_border=self.fig_fl_border.add_subplot(111)
            self.fig_fl_border.subplots_adjust(left=0, right=1, bottom=0, top=1)
            self.ax_fl_border.set_axis_off()
            self.fig_fl_border.add_axes(self.ax_fl_border)
            self.ax_fl_border.imshow(fl_img,cmap="gray")

        self.new_grid(self.d,self.grid,self.shape,self.bf_img)
        self.canvas = FigureCanvas(self.fig_border)

        #Build QDialog with different QWidgets
        self.setWindowTitle("Estimated Grid")
        self.toolbar = MyNavigationToolbar(self.canvas, self, self.ax_border,self.org_ax_border)
        self.toolbar.zoomed.connect(self.on_zoomed)

        self.spin_box1 = QtWidgets.QSpinBox()
        self.spin_box1.setFixedWidth(150)
        self.spin_box1.setMinimum(self.d/2)
        self.spin_box1.setMaximum(self.d*2)
        self.spin_box1.setValue(self.d)
        self.spin_box2 = QtWidgets.QSpinBox()
        self.spin_box2.setFixedWidth(150)
        self.spin_box2.setMinimum(-50)
        self.spin_box2.setMaximum(50)
        self.spin_box2.setValue(0)
        self.spin_box3 = QtWidgets.QSpinBox()
        self.spin_box3.setFixedWidth(150)
        self.spin_box3.setMinimum(-50)
        self.spin_box3.setMaximum(50)
        self.spin_box3.setValue(0)

        self.save_button = QtWidgets.QPushButton("Save")
        self.save_button.setFixedWidth(150)
        self.save_button.setIcon(self.style().standardIcon(getattr(QtWidgets.QStyle, "SP_DialogSaveButton")))
        self.save_button.clicked.connect(self.save_figs)

        self.reset_button = QtWidgets.QPushButton("Reset")
        self.reset_button.setFixedWidth(150)
        self.reset_button.setIcon(self.style().standardIcon(getattr(QtWidgets.QStyle, "SP_DialogResetButton")))
        self.reset_button.clicked.connect(self.reset_grid)
        
        self.measure_button = QtWidgets.QPushButton("Measured Diameter")
        self.measure_button.setFixedWidth(150)
        self.measure_button.clicked.connect(self.measure_distance)
        self.measure_button.setToolTip("Press button to measure the diameter of one grid element (circle: diameter, square: width).\
                                       \nFor doing so please click to the edges of an element.\n\
You can then enter the value in the \"Diameter Size\" field in the home screen \nto automatically adjust the scale factor and start grid detection again.")

        self.distance_label = QtWidgets.QLabel(" Nothing measured yet! ")
        self.distance_label.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)

        label_diameter = QtWidgets.QLabel("Diameter:")
        label_diameter.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        label_dx = QtWidgets.QLabel("\u0394x:")
        label_dx.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        label_dy = QtWidgets.QLabel("\u0394y:")
        label_dy.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)

        spin_box_layout = QtWidgets.QVBoxLayout()
        spin_box_layout.addWidget(label_diameter)
        spin_box_layout.addWidget(self.spin_box1)
        spin_box_layout.addWidget(label_dx)
        spin_box_layout.addWidget(self.spin_box2)
        spin_box_layout.addWidget(label_dy)
        spin_box_layout.addWidget(self.spin_box3)
        spin_box_group = QtWidgets.QGroupBox()
        spin_box_group.setLayout(spin_box_layout)

        button_layout = QtWidgets.QVBoxLayout()
        button_layout.addWidget(self.save_button)
        button_layout.addWidget(self.reset_button)
        button_group = QtWidgets.QGroupBox()
        button_group.setLayout(button_layout)
        
        measure_layout = QtWidgets.QVBoxLayout()
        measure_layout.addWidget(self.measure_button)
        measure_layout.addWidget(self.distance_label)
        measure_group = QtWidgets.QGroupBox()
        measure_group.setLayout(measure_layout)

        line = QtWidgets.QFrame()
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)

        control_layout = QtWidgets.QVBoxLayout()
        control_layout.addWidget(self.toolbar)
        control_layout.addWidget(spin_box_group)
        control_layout.addWidget(button_group)
        control_layout.addWidget(measure_group)

        control_widget = QtWidgets.QWidget()
        control_widget.setLayout(control_layout)
        control_widget.setFixedWidth(300)
        control_widget.setFixedHeight(500)

        self.spin_box1.valueChanged.connect(self.reload_plot)
        self.spin_box2.valueChanged.connect(self.reload_plot)
        self.spin_box3.valueChanged.connect(self.reload_plot)
        
        layout = QtWidgets.QHBoxLayout()
        layout.addWidget(control_widget)
        layout.addWidget(self.canvas)

        self.setLayout(layout)

    def reload_plot(self):
        """
        Reload and update the plot with modified grid parameters.

        Args:
            None

        Returns:
            None

        Note:
            This method reloads and updates the plot by modifying the grid parameters based on user input (diameter, dx, dy).
            It deep copies the original grid, adjusts the coordinates of each point, and then updates the plot with the new grid.
            Finally, it redraws the canvas to reflect the changes.
        """

        diameter = self.spin_box1.value()
        dx = self.spin_box2.value()
        dy = self.spin_box3.value()
        self.grid=copy.deepcopy(self.org_grid)
        for row in self.grid:
                for point in row:
                    point[0]+=dx
                    point[1]-=dy
        self.d=diameter
        self.new_grid(self.d,self.grid,self.shape,self.bf_img)
        self.canvas.draw()

    def reset_grid(self):
        """
        Reset the grid parameters to their original values and update the plot.

        Args:
            None

        Returns:
            None

        Note:
            This method resets the grid parameters (diameter, grid points) to their original values. It also updates the plot
            with the original grid configuration and resets the input fields for diameter, dx, and dy to their original values.
            Finally, it redraws the canvas to reflect the changes.
        """

        self.d = copy.deepcopy(self.org_d)
        self.grid=copy.deepcopy(self.org_grid)
        self.new_grid(self.d,self.grid,self.shape,self.bf_img)
        self.canvas.draw()
        self.spin_box1.setValue(self.d)
        self.spin_box2.setValue(0)
        self.spin_box3.setValue(0)
             
    def new_grid(self,d,grid, shape, bf_img, fl_img=None): 
        """
        Create and update a new grid representation on the plot based on the specified parameters.

        Args:
            d (float): The diameter size of the grid cells.
            grid (list of lists): The grid points represented as a list of (x, y) coordinates.
            shape (str): The shape of the grid cells, either "square" or "circle".
            bf_img (numpy.ndarray): The input brightfield image.
            fl_img (numpy.ndarray): An optional fluorescence image. Default is None.

        Returns:
            None

        Note:
            This method creates and updates a new grid representation on the plot based on the provided diameter, grid points,
            and shape. It also updates the mask with 1s in the regions corresponding to the grid cells. The plot includes
            border markers and cell shapes (squares or circles) with different colors.
        """

        self.ax_border.patches.clear()
        self.ax_mask.patches.clear()
        self.ax_border.lines.clear()
        if fl_img is not None:
            self.ax_fl_border.patches.clear()
            self.ax_fl_mask.patches.clear()
        self.mask = np.zeros(bf_img.shape[0:2])

        for row in grid:
            for point in row:
                x_i = int(point[0])
                y_i = int(point[1])
                self.ax_border.plot(x_i,y_i,'r+', markersize=self.markersize)
                if fl_img is not None:
                    self.ax_fl_border.plot(x_i,y_i,'r+', markersize=self.markersize)

                if shape=="square":
                    rect = plt.Rectangle((x_i - d / 2, y_i - d / 2), d, d, facecolor='none', linewidth=self.linewidth,
                    edgecolor='c', )
                    rect_borders = plt.Rectangle((x_i - d / 2, y_i - d / 2), d, d, facecolor='none', linewidth=2*self.linewidth,
                    edgecolor='r', )
                    start = (int(y_i - d / 2), int(x_i - d / 2))
                    extent = (int(d), int(d))
                    yy, xx = rectangle(start=start, extent=extent, shape=bf_img.shape[0:2])
                    self.mask[yy, xx] = 1

                    self.ax_mask.add_patch(rect)
                    self.ax_border.add_patch(rect_borders)

                    if fl_img is not None:
                        rect = plt.Rectangle((x_i - d / 2, y_i - d / 2), d, d, facecolor='none', linewidth=self.linewidth,
                        edgecolor='c', )
                        rect_borders = plt.Rectangle((x_i - d / 2, y_i - d / 2), d, d, facecolor='none', linewidth=2*self.linewidth,
                        edgecolor='r', )
                        self.ax_fl_mask.add_patch(rect)
                        self.ax_fl_border.add_patch(rect_borders)
                elif shape=="circle":
                    circ = plt.Circle((x_i, y_i), d / 2, fill=False, linewidth=self.linewidth, color='c', )
                    circ_borders = plt.Circle((x_i, y_i), d / 2, fill=False, linewidth=2*self.linewidth, color='r', )
                    yy, xx = disk((y_i, x_i), radius=d / 2, shape=bf_img.shape[0:2])
                    self.mask[yy, xx] = 1

                    self.ax_mask.add_patch(circ) 
                    self.ax_border.add_patch(circ_borders) 
                    if fl_img is not None:
                        circ = plt.Circle((x_i, y_i), d / 2, fill=False, linewidth=self.linewidth, color='c', )
                        circ_borders = plt.Circle((x_i, y_i), d / 2, fill=False, linewidth=2*self.linewidth, color='r', )
                        self.ax_fl_mask.add_patch(circ)
                        self.ax_fl_border.add_patch(circ_borders)

    def on_zoomed(self,zoom_factor):
        """
        Handle zooming by adjusting the linewidth and markersize, then updating the grid representation and redrawing the canvas.

        Args:
            zoom_factor (float): The zoom factor applied to adjust linewidth and markersize.

        Returns:
            None

        Note:
            This method handles zooming by adjusting the linewidth and markersize based on the provided zoom_factor.
            It then updates the grid representation on the plot with the new linewidth and markersize and redraws the canvas.
        """

        self.linewidth=zoom_factor/10
        self.markersize=2*zoom_factor
        self.new_grid(self.d,self.grid,self.shape,self.bf_img)
        self.canvas.draw_idle()

    def measure_distance(self):
        """
        Initialize a distance measurement by updating the label and setting up an event listener for user interaction.

        Args:
            None

        Returns:
            None

        Note:
            This method prepares the interface for measuring distances by updating the label to instruct the user to click on
            the first point. It also sets up an event listener to capture user input.
        """

        self.distance_label.setText("Click on the first point!")
        self.cid = self.canvas.mpl_connect("button_press_event", self.onclick)


    def onclick(self, event):
        """
        Handle user clicks for measuring distances between two points.

        Args:
            event (matplotlib.backend_bases.MouseEvent): The mouse event triggered by a user click.

        Returns:
            None

        Note:
            This method handles user clicks to measure distances between two points on the plot. When the first point is clicked,
            it waits for a second click to determine the distance. Once the distance is calculated, it updates the distance label
            and disconnects the event listener.
        """

        if event.button == 1:
            if hasattr(self, "x1") and hasattr(self, "y1"):
                x2, y2 = event.xdata, event.ydata
                distance = ((self.x1 - x2) ** 2 + (self.y1 - y2) ** 2) ** 0.5
                self.distance_label.setText(f"Diameter: {distance:.2f} px")
                self.canvas.mpl_disconnect(self.cid)
                del self.x1, self.y1
                self.cid = None
            else:
                self.x1, self.y1 = event.xdata, event.ydata
                self.distance_label.setText("Click on the second point!")

    def save_figs(self):
        """
        Save overlay figures and grid parameters to specified directories.

        Args:
            None

        Returns:
            None

        Note:
            This method saves overlay figures (mask with grid overlay and mask borders) as TIFF images to the detection directory.
            It also saves the grid parameters to a binary file. Optionally, it can save fluorescence overlay figures and parameters
            to their respective directories if a fluorescence image is provided.
        """

        self.ax_mask.imshow(self.mask,cmap="RdYlGn",alpha=0.5)
        self.fig_mask.savefig(os.path.join(self.detection_dir, 'overlay_mask.tif'), dpi=200, bbox_inches='tight', pad_inches=0) 
        self.fig_border.savefig(os.path.join(self.detection_dir, 'overlay_mask_borders.tif'), dpi=200, bbox_inches='tight', pad_inches=0)
        self.ax_mask.clear()
        self.ax_mask.imshow(self.bf_img,cmap="gray")

        if self.fl_img is not None:
            self.ax_fl_mask.imshow(self.mask,cmap="RdYlGn",alpha=0.5)
            self.fig_fl_mask.savefig(os.path.join(self.detection_fl_dir, 'overlay_mask.tif'), dpi=200, bbox_inches='tight', pad_inches=0) 
            self.fig_fl_border.savefig(os.path.join(self.detection_fl_dir, 'overlay_mask_borders.tif'), dpi=200, bbox_inches='tight', pad_inches=0)
            self.ax_fl_mask.clear()
            self.ax_fl_mask.imshow(self.fl_img,cmap="gray")

        params = GridParameter(
            self.angle,
            self.grid,
            self.shape,
            self.d
        )
        sv = os.path.join(self.result_dir, "grid.params")
        pickle.dump(params,open(sv,"wb"))

    def get_mask(self):
        """
        Generate and save overlay figures, then return the binary mask.

        Args:
            None

        Returns:
            numpy.ndarray: The binary mask generated based on the grid and saved as an overlay figure.

        Note:
            This method generates overlay figures of the binary mask with the grid overlay and mask borders, saves them to
            the detection directory, and then returns the binary mask.
        """

        self.save_figs()
        return self.mask

class MyNavigationToolbar(NavigationToolbar2QT):
    """
    Customized navigation toolbar for zooming with PyQt integration.

    Attributes:
        zoomed (PyQtSignal): Signal emitted when zooming occurs.

    Args:
        canvas (matplotlib.backends.backend_qt5.FigureCanvasQTAgg): The canvas to which the toolbar is attached.
        parent (QWidget): The parent widget.
        ax_border (matplotlib.axes.Axes): The border axes to be synchronized with zoom.
        org_ax_border (matplotlib.axes.Axes): The original border axes for zoom reference.

    Methods:
        __init__(canvas, parent, ax_border, org_ax_border):
            Initialize the custom navigation toolbar.
        _update_view(*args):
            Update the view and emit the zoom factor signal.
        release_zoom(*args):
            Release the zoom and emit the zoom factor signal.
    """

    zoomed = QtCore.pyqtSignal(float)

    def __init__(self, canvas, parent,ax_border, org_ax_border):
        super().__init__(canvas, parent)

        self.ax_border=ax_border
        self.org_ax_border=org_ax_border
        self.org_xlim=self.org_ax_border.get_xlim()
        self.org_ylim=self.org_ax_border.get_ylim()

    def _update_view(self, *args):
        """
        Update the view and emit the zoom factor signal.

        Args:
            *args: Variable-length argument list.

        Returns:
            None
        """

        super()._update_view(*args)
        updated_xlim = self.ax_border.get_xlim()
        updated_ylim = self.ax_border.get_ylim()
        
        zoom_factor_x =  (self.org_xlim[1] - self.org_xlim[0]) / (updated_xlim[1] - updated_xlim[0])
        zoom_factor_y = (self.org_ylim[1] - self.org_ylim[0]) / (updated_ylim[1] - updated_ylim[0])
        zoom_factor = math.sqrt(zoom_factor_x * zoom_factor_y)
        self.zoomed.emit(zoom_factor)

    def release_zoom(self, *args):
        """
        Release the zoom and emit the zoom factor signal.

        Args:
            *args: Variable-length argument list.

        Returns:
            None
        """
        
        super().release_zoom(*args)
        updated_xlim = self.ax_border.get_xlim()
        updated_ylim = self.ax_border.get_ylim()
        
        zoom_factor_x =  (self.org_xlim[1] - self.org_xlim[0]) / (updated_xlim[1] - updated_xlim[0])
        zoom_factor_y = (self.org_ylim[1] - self.org_ylim[0]) / (updated_ylim[1] - updated_ylim[0])
        zoom_factor = math.sqrt(zoom_factor_x * zoom_factor_y)
        self.zoomed.emit(zoom_factor)


