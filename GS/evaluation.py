import pickle
import numpy as np
from sklearn.neighbors import NearestNeighbors


file_name_gt = "/home/ws/sc1357/data/2021_09_02_DMA_Spot_Segmentation/eval/DMA_672/gt_672_1_20210429_pHMeasurements_HeLaCells_24h.params"
file_name_screener = "/home/ws/sc1357/data/2021_09_02_DMA_Spot_Segmentation/eval/DMA_672/672_1_20210429_pHMeasurements_HeLaCells_24h.params"

with open(file_name_gt, "rb") as file:
    gt_params = pickle.load(file)

with open(file_name_screener, "rb") as file:
    screener_params = pickle.load(file)


gamma_d = np.abs(screener_params.diameter-gt_params.diameter)/gt_params.diameter

points = np.zeros((screener_params.grid.shape[0]*screener_params.grid.shape[1],2))
i = 0

for row in screener_params.grid:
    for point in row:
        points[i,0:2] = point[0:2].copy()  
        if i < 5:
            print(points[i,0:2])
        i +=1

nbrs = NearestNeighbors(n_neighbors=1, algorithm='ball_tree').fit(points)

i = 0  
dst_lst = list()

norm_factor = gt_params.grid[1,0,1]-gt_params.grid[0,0,1]

#gt_params.grid = np.delete(gt_params.grid,np.arange(1,48), axis=1)
i = 0
#print(norm_factor)
for row in gt_params.grid:
    for point in row:
        if i<5:
            print(point[0:2])
        i+=1
        dist, _     = nbrs.kneighbors(point[0:2].reshape(1, -1), return_distance=True)
        dst_lst.append(dist.squeeze())

gamma_p = np.mean(dst_lst)/norm_factor

#print(f"gamma_p: {gamma_p}")
#print(f"gamma_d: {gamma_d}")