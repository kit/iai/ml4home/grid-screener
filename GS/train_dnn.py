import logging
from argparse import Namespace

import wandb
import yaml
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks.early_stopping import EarlyStopping

from GS.dnn.data_processing.seg_processing import ImgSegProcessingPipeline, SemanticSegmentationProccesor
from GS.dnn.dataset.spot_datamodule import SpotDataModule
from GS.utils.mapping import get_model
from GS.utils.param_handling import split_parameters

config_path = "/home/ws/sc1357/projects/devel/src/grid-screener/GS/config/cfg-unet-train.yaml"

logging.info("Loading DNN model...")
cfg_yaml = yaml.load(config_path, Loader=yaml.FullLoader)
wandb.init(
    config=cfg_yaml,
    allow_val_change=True,
    project=None,
    mode="disabled",
)
config = wandb.config

parameters_splitted = split_parameters(config, ["model", "data", "train", "spot_detection"])
model_params = split_parameters(parameters_splitted["model"])["parameters"]
img_proc_params = split_parameters(parameters_splitted["data"])["img_processing"]
spot_detection_params = split_parameters(parameters_splitted["spot_detection"])["parameters"]

trainer_params = split_parameters(parameters_splitted["train"])["trainer"]
callback_params = split_parameters(parameters_splitted["train"])["callbacks"]

dm_params = split_parameters(parameters_splitted["data"])["datamodule"]

test_trafo  = [SemanticSegmentationProccesor(ImgSegProcessingPipeline(Namespace(
                                **img_proc_params)).get_val_transform())]
val_trafo   = [SemanticSegmentationProccesor(ImgSegProcessingPipeline(Namespace(
                                **img_proc_params)).get_val_transform())]
train_trafo = [SemanticSegmentationProccesor(ImgSegProcessingPipeline(Namespace(
                                **img_proc_params)).get_train_transform())]

datamodule = SpotDataModule(
                    **{
                        "train_transforms": train_trafo,
                        "val_transforms": val_trafo,
                        "test_transforms": test_trafo,
                    },
                **dm_params
            )

model = get_model("unet")(**model_params)

callback_lst = list()

early_stopping_cb = EarlyStopping(
                monitor='val/loss',
                patience=callback_params["early_stopping_patience"],
                verbose=True,
                mode='min'
    )

callback_lst.append(early_stopping_cb)

trainer = Trainer(
            **trainer_params,
            callbacks=callback_lst
        )

trainer.fit(model, datamodule)
trainer.test(datamodule)