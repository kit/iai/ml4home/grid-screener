import logging
from argparse import Namespace

import cv2
import numpy as np
import slidingwindow as sw
import tifffile
import torch
import wandb
import yaml

from GS.dnn.data_processing.seg_processing import ImgSegProcessingPipeline, SemanticSegmentationProccesor
from GS.utils.mapping import get_model
from GS.utils.param_handling import split_parameters
from GS.dnn.objective.dice import DiceLoss
import albumentations as A
from albumentations.pytorch import ToTensorV2


class DnnProcessor:
    """
    Class for processing data using a Deep Neural Network (DNN) model.

    Args:
        checkpoint_path (str): The path to the checkpoint file of the trained DNN model.
        config_path (str): The path to the configuration file containing model and data parameters.

    Attributes:
        checkpoint_path (str): The path to the checkpoint file of the trained DNN model.
        config_path (str): The path to the configuration file containing model and data parameters.
        use_gpu (bool): Flag indicating whether the DNN model should use the GPU for processing.
        model (torch.nn.Module): The DNN model loaded from the checkpoint.
        test_trafo (Callable): A callable transformation for preprocessing input images.
    """

    def __init__(self, checkpoint_path, config_path) -> None:
        logging.info("Loading DNN model...")
        self.checkpoint_path = checkpoint_path
        self.config_path = config_path
        cfg_yaml = yaml.load(self.config_path, Loader=yaml.FullLoader)
        wandb.init(
            config=cfg_yaml,
            allow_val_change=True,
            project=None,
            mode="disabled",
        )
        config = wandb.config

        parameters_splitted = split_parameters(config, ["model", "data", "spot_detection"])

        model_params = split_parameters(parameters_splitted["model"])["params"]
        img_proc_params = split_parameters(parameters_splitted["data"])["img_processing"]
        spot_detection_params = split_parameters(parameters_splitted["spot_detection"])["parameters"]

        self.use_gpu        = spot_detection_params["use_gpu"]
        
        self.model = get_model("unetsemantic").load_from_checkpoint(checkpoint_path=self.checkpoint_path,**(model_params))

        self.test_trafo = SemanticSegmentationProccesor(ImgSegProcessingPipeline(Namespace(
                                        **img_proc_params)).get_val_transform())

        if self.use_gpu:
            self.model.cuda()

        self.model.eval()
        logging.info("...finished")

    def predict(self,image, scale_factor,origin, window_size=512):
        """
        Generate a prediction mask for the input image using the specified scale factor, origin, and window size.

        Args:
            self (object): The instance of the class.
            image (numpy.ndarray): The input image as a NumPy array.
            scale_factor (float): The scale factor by which to resize the image.
            origin (str): The origin or transformation method for the image.
            window_size (int, optional): The size of the analysis window. Defaults to 512.

        Returns:
            numpy.ndarray: A prediction mask as a NumPy array.

        Note:
            This method takes an input image, optionally resizes it based on the scale factor,
            transforms it according to the specified origin, and generates a prediction mask
            for the image. The analysis is performed using a sliding window approach with
            overlapping windows.
        """

        if(len(image.shape)==3):
           image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        image_0 = image.copy()
        height_0, width_0 = image_0.shape[0:2]
        dim_0 = (width_0, height_0)

        self.scale_factor=scale_factor
        if self.scale_factor<1:
            target_width    = int(width_0 * self.scale_factor)
            target_height   = int(height_0 * self.scale_factor)
            dim = (target_width, target_height)
            image = cv2.resize(image, dim)
        else:
            target_width    = width_0
            target_height   = height_0

        if origin=="GS-kaida-GS":
            image_tensor=self.transformImg(image, target_height, target_width)
        else:
            image_tensor,_,_ = self.test_trafo(image)

        prediction = torch.zeros_like(image_tensor)
        prediction = prediction.squeeze()

        windows = sw.generate(data=image_tensor.numpy(),
                                dimOrder=sw.DimOrder.ChannelHeightWidth,
                                maxWindowSize=window_size,
                                overlapPercent=0.1)

        for window in windows:
            h = 12 if window.indices()[1].start > 0 else 0
            w = 12 if window.indices()[2].start > 0 else 0
            hmin = window.indices()[1].start + h
            wmin = window.indices()[2].start + w
            net_input_window = image_tensor[window.indices()].cuda() if self.use_gpu else image_tensor[window.indices()]
            prediction_window = self.model(net_input_window.unsqueeze(0)).detach().cpu() if self.use_gpu else self.model(net_input_window.unsqueeze(0)).detach()
            prediction[hmin:window.indices()[1].stop, wmin:window.indices()[2].stop] = prediction_window[0,0,h:, w:]

        prediction  = 255*cv2.resize((prediction.numpy()>0.5).astype(np.uint8), dim_0, interpolation=cv2.INTER_NEAREST)

        return prediction

    def transformImg(self, img,height, width):
        """
        Transform and preprocess the input image.

        Args:
            self (object): The instance of the class.
            img (numpy.ndarray): The input image as a NumPy array.
            height (int): The desired height for the transformed image.
            width (int): The desired width for the transformed image.

        Returns:
            torch.Tensor: A transformed and preprocessed image as a PyTorch tensor.

        Note:
            This method takes an input image and applies a series of transformations and
            preprocessing steps. First, it normalizes the image using the `normalize_percentile`
            method. Then, it resizes the image to the specified `height` and `width`. Finally,
            it converts the transformed image to a PyTorch tensor and returns it.
        """

        normalized_image = self.normalize_percentile(img)
        transform = A.Compose([A.Resize(height, width),ToTensorV2()])
        transformed_img = transform(image=normalized_image)["image"]
        transformed_img = transformed_img.float()
        return transformed_img
    
    def normalize_percentile(self,img):
        """
        Normalize the input image using the 1st and 99th percentiles.

        Args:
            self (object): The instance of the class.
            img (numpy.ndarray): The input image as a NumPy array.

        Returns:
            numpy.ndarray: The normalized image as a NumPy array.

        Note:
            This method performs percentile-based normalization on the input image. It calculates
            the 1st and 99th percentiles of the image intensity values, clips the values below
            the 1st percentile to the 1st percentile value and above the 99th percentile to the
            99th percentile value, and then scales the values to the range [0, 1].
        """

        min_val = np.percentile(img, 1)
        max_val = np.percentile(img, 99)
        img[img < min_val] = min_val
        img[img > max_val] = max_val
        img = (img - min_val) / (max_val - min_val)
        return img

if __name__ == "__main__":
    checkpoint_path = "/home/ws/sc1357/data/trained_ann/2021_09_03_dma_spots_train/dnn_weights.ckpt"
    config_path     = "/home/ws/sc1357/projects/devel/src/grid-screener/GS/config/cfg-unet.yaml"

    dl_processor = DnnProcessor(checkpoint_path,config_path)
    image = tifffile.imread("/home/ws/sc1357/data/2021_04_30_ph_value_estimation/20210630_pHMeasurement_Michael_Hela_24h_672slide.tif")
    dl_processor.predict(image)