import os
import random

import pytorch_lightning as pl
from torch.utils.data import DataLoader

from GS.dnn.dataset.spot_dataset import SpotDataset
from GS.utils.dataloading import (
    custom_collate,
    seed_worker
)


class SpotDataModule(pl.LightningDataModule):
    def __init__(
        self,
        root_dir: str,
        batch_size: int,
        val_to_train_ratio,
        train_transforms=None,
        val_transforms=None,
        test_transforms=None,
        num_workers=0,
        pin_memory=False,
        shuffle=True,
        drop_last=False,
    ):
        super().__init__()
        self.batch_size         = batch_size
        self.val_to_train_ratio = val_to_train_ratio
        self.num_workers        = num_workers
        self.pin_memory         = pin_memory
        self.shuffle            = shuffle
        self.drop_last          = drop_last
        self.root_dir           = root_dir
        self.train_root_dir     = os.path.join(self.root_dir, "train")
        self.test_root_dir      = os.path.join(self.root_dir, "test")
        self.train_transforms   = train_transforms
        self.val_transforms     = val_transforms
        self.test_transforms    = test_transforms
        self.train_dataset: SpotDataset = None
        self.val_dataset: SpotDataset = None
        self.test_dataset: SpotDataset = None
        self.__init_datasets()
        self.__init_val_dataset()
        
    def __init_datasets(self):
        self.train_dataset = SpotDataset(
            root_dir=self.train_root_dir, 
            transforms=self.train_transforms,
        )

        self.val_dataset = SpotDataset(
            root_dir=self.train_root_dir, 
            transforms=self.val_transforms,
            empty_dataset=True
        )

        self.test_dataset = SpotDataset(
            root_dir=self.test_root_dir, 
            transforms=self.test_transforms,
        )

    def train_dataloader(self):
        return DataLoader(
            self.train_dataset,
            batch_size=self.batch_size,
            collate_fn=custom_collate,
            num_workers=self.num_workers,
            pin_memory=self.pin_memory,
            worker_init_fn=seed_worker,
            shuffle=self.shuffle,
            drop_last=self.drop_last
        )

    def val_dataloader(self):
        return DataLoader(
            self.val_dataset,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
            pin_memory=self.pin_memory,
            worker_init_fn=seed_worker
        )
  
    def test_dataloader(self):
        return DataLoader(
            self.test_dataset,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
            pin_memory=self.pin_memory,
            worker_init_fn=seed_worker
        )

    def __init_val_dataset(self):
        num_val_samples = int(round(len(self.train_dataset) * (self.val_to_train_ratio)))
        num_val_samples = num_val_samples if num_val_samples > 0 else 1
        for _ in range(
            num_val_samples
        ):
            self.val_dataset.add_sample(
                self.train_dataset.pop_sample(
                    random.randrange(len(self.train_dataset))
                )
            )