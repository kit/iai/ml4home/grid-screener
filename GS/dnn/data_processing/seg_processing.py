import albumentations as A
import cv2
import numpy as np
from albumentations.pytorch.transforms import ToTensorV2


def list_transform(argument):
    return list(argument) if isinstance(argument,np.ndarray) else argument

def norm_std_mean(image, mean=None, std=None):
    image = image.astype("float32")

    if mean is None or std is None:
        mean = np.mean(image, axis=(0,1))
        std  = np.std(image, axis=(0,1))

    denominator = np.reciprocal(std+np.finfo(float).eps, dtype=np.float32)
    image = image.astype(np.float32)
    image -= mean
    image *= denominator
    return image


class ImgSegProcessingPipeline:
    def __init__(self, params):
        self.params = params
        self.params.max_value = None
        self.trafo_dict = dict()
        self.trafo_dict["pre"] = self.make_pre_transform()
        self.trafo_dict["aug"] = self.make_aug_transform()
        self.trafo_dict["norm"] = self.make_norm_transform()
        self.trafo_dict["post"] = self.make_post_transform()
    
    def make_norm_transform(self):
        norm_dict = dict()

        if (hasattr(self.params, 'norm_type')):
            norm_dict["type"] = self.params.norm_type

            if  hasattr(self.params, 'img_norm_mean') and \
                hasattr(self.params, 'img_norm_std'):
                    norm_dict["params"] = dict()
                    norm_dict["params"]["mean"] = self.params.img_norm_mean
                    norm_dict["params"]["std"] = self.params.img_norm_std
            else:
                    norm_dict["params"] = None
            return norm_dict
        return None


    def make_pre_transform(self):
        transform = []

        if self.params.img_type == "mono_16_bit":
            self.params.max_value = 1.0
            transform.append(A.ToFloat(max_value=65535.0))
        elif self.params.img_type == "mono_8_bit":
            self.params.max_value = 1.0
            transform.append(A.ToFloat(max_value=255.0))
        elif self.params.img_type == "rgb_8_bit":
            self.params.max_value = 255.0

        if hasattr(self.params, 'img_size'):
            transform.append(
                A.Resize(height=self.params.img_size[0],
                         width=self.params.img_size[1])
            )

        return self.get_transform(transform, replay=False)

    def make_aug_transform(self,params=None):
        transform = []
        if hasattr(params, 'aug_flip_prob'):
                    enabled=True
                    if hasattr(params,'flip_enabled') and not params.flip_enabled:
                        enabled=False
                    if enabled:
                        transform.append(A.VerticalFlip(p=params.aug_flip_prob))
                        transform.append(A.HorizontalFlip(p=params.aug_flip_prob))

        if  hasattr(params, 'aug_gauss_noise_var_limit') and \
            hasattr(params, 'aug_gauss_noise_prob'):
                transform.append(
                    A.GaussNoise(
                        var_limit=params.aug_gauss_noise_var_limit,
                        p=params.aug_gauss_noise_prob,
                    )
                )

        if  hasattr(params, 'aug_shift_scale_rotate_shift_lim') and \
            hasattr(params, 'aug_shift_scale_rotate_scale_lim') and \
            hasattr(params, 'aug_shift_scale_rotate_rot_lim') and \
            hasattr(params, 'aug_shift_scale_rotate_prob'):
                transform.append(
                    A.ShiftScaleRotate(
                        shift_limit=params.aug_shift_scale_rotate_shift_lim,
                        scale_limit=params.aug_shift_scale_rotate_scale_lim,
                        rotate_limit=params.aug_shift_scale_rotate_rot_lim,
                        p=params.aug_shift_scale_rotate_prob,
                    )
                )

        if  hasattr(params, 'aug_rand_brightness_contrast_prob') and \
            hasattr(params, 'aug_rand_brightness_contrast_brightness_limit') and \
            hasattr(params, 'aug_rand_brightness_contrast_contrast_limit'):
                transform.append(
                    A.RandomBrightnessContrast(
                        p=params.aug_rand_brightness_contrast_prob,
                        brightness_limit=params.aug_rand_brightness_contrast_brightness_limit,
                        contrast_limit=params.aug_rand_brightness_contrast_contrast_limit,
                        )
                )

        if  hasattr(params, 'random_resized_crop_size') and \
            hasattr(params, 'random_resized_scale') and \
            hasattr(params, 'random_resized_ratio') and \
            hasattr(params, 'random_resized_propability'):
                enabled=True
                if hasattr(params,'random_resized_crop_enabled') and not params.random_resized_crop_enabled:
                    enabled=False
                if enabled:
                    transform.append(
                            A.RandomResizedCrop(
                                height=params.random_resized_crop_size[0],
                                width=params.random_resized_crop_size[1],
                                scale=params.random_resized_scale,
                                ratio=params.random_resized_ratio,
                                interpolation=cv2.INTER_CUBIC,
                                p=params.random_resized_propability
                            )
                        )

        if  hasattr(params, 'gaussian_blur_prop') and \
            hasattr(params, 'gaussian_blur_kernel_size') and \
            hasattr(params, 'std_dev_range'):
                enabled=True
                if hasattr(params,'gaussian_blur_enabled') and not params.gaussian_blur_enabled:
                    enabled=False
                if enabled:
                    transform.append(
                        A.GaussianBlur(
                            p=params.gaussian_blur_prop,
                            blur_limit = tuple(params.gaussian_blur_kernel_size),
                            sigma_limit=tuple(params.std_dev_range)
                        )
                    )   

        return self.get_transform(transform)

    def make_post_transform(self):
        transform = []

        transform.append(A.ToFloat())
        transform.append(ToTensorV2())

        return self.get_transform(transform, replay=False)

    def get_transform(self, trafo_lst, replay=False):
        if replay:
            return A.ReplayCompose(trafo_lst)
        else:
            return A.Compose(trafo_lst)

    def get_val_transform(self):
        trafo_dict = self.trafo_dict.copy()
        trafo_dict["aug"] = None
        return trafo_dict

    def get_train_transform(self):
        trafo_dict = self.trafo_dict.copy()
        return trafo_dict

    def get_pre_transform(self):
        trafo_dict = self.trafo_dict.copy()
        trafo_dict["aug"] = None
        trafo_dict["post"] = None
        return trafo_dict


class SemanticSegmentationProccesor:
    def __init__(self, transform: dict()):
        if ("pre" not in transform or
                "aug" not in transform or
                "post" not in transform):
            raise Exception("Wrong dict structure")
        self.transform = transform

    def __call__(self, image, mask=None):
        if mask is None:
            mask = np.zeros((image.shape))
        transformed_data = self.transform["pre"](image=image, mask=mask)
        sample_img, label_one_hot = (
            transformed_data["image"],
            transformed_data["mask"],
        )

        mean, std = np.mean(sample_img, axis=(0,1)), np.std(sample_img, axis=(0,1))

        if self.transform["aug"] is not None:
            transformed_data = self.transform["aug"](
                image=sample_img, mask=label_one_hot
            )
            sample_img, label_one_hot, transformations = (
                transformed_data["image"],
                transformed_data["mask"],
                transformed_data["replay"] if "replay" in transformed_data else []
            )
        else:
            transformations = []

        if self.transform["norm"] is not None:
            if self.transform["norm"]["type"]=="per_image":
                sample_img = norm_std_mean(sample_img)
            elif self.transform["norm"]["type"]=="per_dataset":
                sample_img = norm_std_mean(
                    sample_img, 
                    mean=self.transform["norm"]["params"]["mean"],
                    std=self.transform["norm"]["params"]["std"])
        
        if self.transform["post"] is not None:
            transformed_data = self.transform["post"](
                image=sample_img, mask=label_one_hot
            )
            sample_img, label_one_hot = (
                transformed_data["image"],
                transformed_data["mask"],
            )
        return sample_img, label_one_hot, transformations