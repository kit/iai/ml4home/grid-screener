import os

import numpy as np
import tifffile
from skimage.transform import rotate

from GS.processing.processing_fcn import ProcessingFcn


class CropSpots(ProcessingFcn):
    def __init__(self, selected_img_type):
        super(CropSpots,self).__init__()
        self.selected_img_type = selected_img_type
    
    def run_process(self,bf_img,params,fl_img=None):
        angle   =params.get_angle()
        d = params.get_diameter()
        grid = params.get_grid()
        self.grid = params.get_grid()
        shape = params.get_shape()

        if self.selected_img_type == "FL" and fl_img is not None:
            img=rotate(fl_img,angle,resize=True, preserve_range=True, order=0).astype(fl_img.dtype)
        else:
            img=rotate(bf_img,angle,resize=True, preserve_range=True, order=0).astype(bf_img.dtype)

        spot_lst = list()
        for row in grid:
            for point in row:
                x_i = point[0]
                y_i = point[1]
                spot_lst.append(img[int(y_i-d/2):int(y_i+d/2),int(x_i-d/2):int(x_i+d/2)])

        return spot_lst

    def export_results(self,processing_dir,results, bf_img,params,fl_img=None):
        i_r = list(range(self.grid.shape[0]))
        i_c = list(range(self.grid.shape[1]))
        mesh = np.array(np.meshgrid(i_r, i_c))
        in_data = mesh.T.reshape(-1, 2)

        for i, spot in enumerate(results):
            i_r,i_c=in_data[i]
            tifffile.imwrite(os.path.join(processing_dir, f"spot_row_{str(i_r).zfill(2)}_col{str(i_c).zfill(2)}.tif"), spot)
