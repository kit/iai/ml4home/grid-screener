import os

import cv2
import matplotlib.pyplot as plt
import numpy as np
import tifffile
from skimage import measure
from skimage.transform import rotate
import torch
from skimage.transform import rescale
from DLIP.utils.helper_functions.window_inference import window_inference
from skimage import exposure

from GS.processing.processing_fcn import ProcessingFcn

def normalize(img):
    return (img - img.min()) / (img.max() - img.min())

class CellsPerSpotDL(ProcessingFcn):
    def __init__(
        self,
        model_path
    ):
        super(CellsPerSpotDL,self).__init__()
        self.model = torch.load(model_path)
        self.model.eval()

        if torch.cuda.is_available():
            self.model.cuda()

    def run_process(self,bf_img,params,fl_img=None):
        angle   = params.get_angle()

        fl_img = rotate(fl_img,angle,resize=True, preserve_range=True, order=0).astype(fl_img.dtype)

        d       = params.get_diameter()
        grid    = params.get_grid()

        cells_per_spot = np.zeros((grid.shape[0],grid.shape[1]))
        crop_list=list()

        for i_r in range(grid.shape[0]):
            for i_c in range(grid.shape[1]):
                x_i = grid[i_r,i_c,0]
                y_i = grid[i_r,i_c,1]

                crop = fl_img[int(y_i - d / 2):int(y_i + d / 2), int(x_i - d / 2):int(x_i + d / 2)].copy()
                mask = self._instance_segmentation(crop)

                r_props = measure.regionprops(measure.label(mask))

                cells_per_spot[i_r,i_c] = len(r_props)
                crop = cv2.cvtColor((normalize(crop)*255).astype("uint8"),cv2.COLOR_GRAY2RGB)
                for prop in r_props:
                    crop = cv2.drawMarker(crop, (int(prop.centroid[1]),int(prop.centroid[0])), color=(255,0,0), markerType=cv2.MARKER_CROSS, thickness=1, markerSize=5)
                crop_list.append(crop)
        crop_list=np.asarray(crop_list, dtype=object)
        return {"counted": cells_per_spot, "crop" : crop_list.reshape(grid.shape[0],grid.shape[1])}

    def _instance_segmentation(self, crop):
        shape = crop.shape
        crop = cv2.resize(crop, (shape[1]*4, shape[0]*4,), interpolation=cv2.INTER_LINEAR)

        crop = exposure.equalize_adapthist(crop,)
        #crop = normalize(crop)

        input_tensor = torch.from_numpy(crop).float().unsqueeze(0)

        pred_raw = window_inference(self.model,input_tensor,window_size=512,use_gpu=torch.cuda.is_available()).numpy().squeeze()

        pred_raw = cv2.resize(pred_raw, (shape[1], shape[0]), interpolation=cv2.INTER_LINEAR)
        pred_inst = self.model.post_pro.process(pred_raw, None)
    
        return pred_inst

    def export_results(self,processing_dir,results, bf_img,params,fl_img=None):
        d       = params.get_diameter()
        grid    = params.get_grid()
        shape   = params.get_shape()
        angle   = params.get_angle()

        cells_per_spot = results["counted"]

        patch_dir = os.path.join(processing_dir, "patches")
        
        if not os.path.exists(patch_dir):
            os.makedirs(patch_dir)

        org = (0, 12)
        font = cv2.FONT_HERSHEY_SIMPLEX
        font_scale = 0.4
        for i_r in range(grid.shape[0]):
            for i_c in range(grid.shape[1]):
                crop=results["crop"][i_r,i_c]

                text = f"r: {i_r}, c: {i_c}, num: {cells_per_spot[i_r,i_c]}"
                crop = cv2.putText(crop, text, org, font, font_scale, color=(255,0,0))
                
                tifffile.imwrite(os.path.join(patch_dir, f"spot_row_{str(i_r).zfill(2)}_col{str(i_c).zfill(2)}.tif"), crop)
        
        info = f"cells per spot (row,column) \n" \
               f"contact: marcel.schilling@kit.edu (IAI)"
        output_dec_format = "%." + str(1) + "f"
        file_name = os.path.join(processing_dir, f"cells_per_spot.txt")
        np.savetxt(file_name, cells_per_spot, fmt=output_dec_format, delimiter=' ', newline='\n', header=info, footer='',
                   comments='# ')

        fig, ax = plt.subplots()
        title = f"Cells per spots"
        fig.suptitle(title)
        cps_plt = ax.imshow(cells_per_spot.astype("uint8"), cmap="gray")
        cbar = fig.colorbar(cps_plt, extend='both', shrink=0.9, ax=ax)
        cbar.set_label('#num cells per spot')
        ax.set_xlabel("columns")
        ax.set_ylabel("rows")
        fig.tight_layout()
        fig_name = os.path.join(processing_dir, f"plt_cells_per_spot.png")
        plt.savefig(fig_name, dpi=300)
        plt.close()