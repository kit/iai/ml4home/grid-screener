import os
from PyQt5 import QtWidgets, uic
from PyQt5.QtGui import *

import matplotlib.pyplot as plt
import numpy as np
from skimage.draw import disk, rectangle
from skimage.transform import rotate

from GS.processing.processing_fcn import ProcessingFcn

class ChannelSelectionUi(QtWidgets.QDialog):
    def __init__(self):
        super(ChannelSelectionUi, self).__init__()
        path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'channel_selection.ui')
        uic.loadUi(path, self)    
        self.channels.addItems(['Red', 'Green',  'Blue' ])
    
    def close_app(self):
        self.close()

class MeanIntensityFluorescence(ProcessingFcn):
    def __init__(self):
        super(MeanIntensityFluorescence,self).__init__()
        self.ui = ChannelSelectionUi()
        self.ui.exec_()

        self.channel_index = None
  
    def run_process(self,bf_img,params,fl_img):
        angle   = params.get_angle()

        self.channel_index =  self.ui.channels.currentIndex() 

        # extract only blue channel
        fl_img_single = fl_img[:, :, self.channel_index].copy()
        fl_img_single=rotate(fl_img_single,angle,resize=True, preserve_range=True, order=0).astype("uint8")

        mean_intensity_per_spot   = self._mean_intensity_per_spot(fl_img_single, params)

        return mean_intensity_per_spot

    def _mean_intensity_per_spot(self, fl_img_single, params):
        d       = params.get_diameter()
        grid    = params.get_grid()
        shape   = params.get_shape()

        mean_intensity_per_spot = np.zeros((grid.shape[0],grid.shape[1]))

        for i_r in range(grid.shape[0]):
            for i_c in range(grid.shape[1]):
                mask_i = np.zeros_like(fl_img_single, dtype=bool)
                x_i = grid[i_r,i_c,0]
                y_i = grid[i_r,i_c,1]
                if shape == "square":
                    start = (int(y_i - d / 2), int(x_i - d / 2))
                    extent = (int(d), int(d))
                    yy, xx = rectangle(start=start, extent=extent, shape=fl_img_single.shape[0:2])
                    mask_i[yy, xx] = True
                elif shape == "circle":
                    yy, xx = disk((y_i, x_i), radius=d / 2, shape=fl_img_single.shape[0:2])
                    mask_i[yy, xx] = True

                mean_intensity_per_spot[i_r,i_c] = np.mean(fl_img_single[mask_i])

        return mean_intensity_per_spot


    def export_results(self,processing_dir,results, bf_img,params,fl_img=None):
        info = f"mean instensity per spot (row,column) \n" \
               f"contact: marcel.schilling@kit.edu (IAI)"
        output_dec_format = "%." + str(1) + "f"

        file_name = os.path.join(processing_dir, f"mean_intensity_per_spot.txt")
        np.savetxt(file_name, results, fmt=output_dec_format, delimiter=' ', newline='\n', header=info, footer='',
                   comments='# ')

        fig, ax = plt.subplots()
        title = f"mean intensity per spot"
        fig.suptitle(title)
        cps_plt = ax.imshow(results.astype("uint8"), cmap="gray")
        cbar = fig.colorbar(cps_plt, extend='both', shrink=0.9, ax=ax)
        cbar.set_label('#mean intensity per spot')
        ax.set_xlabel("columns")
        ax.set_ylabel("rows")
        fig.tight_layout()
        fig_name = os.path.join(processing_dir, f"plt_mean_intensity_per_spot.png")
        plt.savefig(fig_name, dpi=300)
        plt.close()