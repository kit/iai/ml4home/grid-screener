import os

import numpy as np
import tifffile
from skimage import measure
from skimage.color import label2rgb
from skimage.transform import rotate

from DLIP.utils.loading.load_transforms import load_transforms
from DLIP.utils.loading.load_model import load_model
from DLIP.utils.loading.merge_configs import merge_configs
from DLIP.utils.loading.split_parameters import split_parameters
from DLIP.utils.loading.initialize_wandb import initialize_wandb
from pytorch_lightning.utilities.seed import seed_everything
import torch
from DLIP.utils.helper_functions.window_inference import window_inference
import tarfile
import cv2
from pathlib import Path

from GS.processing.processing_fcn import ProcessingFcn

def get_model_trafo(tar_file):
    tar = tarfile.open(tar_file, "r:")
    base_path = os.path.dirname(tar_file)
    tar.extractall(path=base_path)
    tar.close()

    # load model
    cfg_yaml = merge_configs(os.path.join(base_path, "cfg.yaml"))

    config = initialize_wandb(
        cfg_yaml=cfg_yaml,
        disabled=True,
        experiment_dir=None,
        config_name=None,
    )

    seed_everything(seed=cfg_yaml['experiment.seed']['value'])
    parameters_splitted = split_parameters(config, ["model", "train", "data"])

    print(os.path.join(base_path, "dnn_weights.ckpt"))

    model = load_model(
        parameters_splitted["model"], 
        checkpoint_path_str=Path(os.path.join(base_path, "dnn_weights.ckpt"))
    )

    if torch.cuda.is_available():
        model.cuda()
    model.eval()

    _, _, test_trafos = load_transforms(parameters_splitted["data"])

    os.remove(os.path.join(base_path, "dnn_weights.ckpt"))
    os.remove(os.path.join(base_path, "cfg.yaml"))

    return model, test_trafos


def normalize(img):
    min_val = np.min(img)
    max_val = np.max(img)
    if min_val!=max_val:
        img_norm = (img-min_val)/(max_val-min_val)
    return (img_norm*255).astype(np.uint8)


class SpheroidAnalysis(ProcessingFcn):
    def __init__(self, dnn_path):
        super(SpheroidAnalysis,self).__init__()
        self.model, self.trafo = get_model_trafo(dnn_path)

    def run_process(self,bf_img,params,fl_img=None):
        try:
            bf_img = bf_img[:,:,0]
        except:
            pass
        angle   = params.get_angle()

        bf_img = rotate(bf_img,angle,resize=True, preserve_range=True, order=0).astype("uint8")

        crops, masks, properties = self._predict_spheroids(bf_img, params)

        return {"properties": properties, "masks": masks, "crops" : crops}


    def _predict_spheroids(self, bf_img, params):
        d       = params.get_diameter()*1.1
        grid    = params.get_grid()

        mask_list = list()
        crop_list = list()
        property_list = list()

        for i_r in range(grid.shape[0]):
            for i_c in range(grid.shape[1]):
                x_i = grid[i_r,i_c,0]
                y_i = grid[i_r,i_c,1]

                crop = bf_img[int(y_i - d / 2):int(y_i + d / 2), int(x_i - d / 2):int(x_i + d / 2)].copy()

                input_tensor,_,_ = self.trafo[0](crop)
                prediction = window_inference(self.model,input_tensor, n_classes=1, window_size=512, use_gpu=torch.cuda.is_available()).squeeze().numpy()

                mask = (prediction > 0.5).astype(np.uint8)

                property_crop, mask_post_pro = self._calculate_properties(mask)
                
                crop_list.append(crop)
                mask_list.append(mask_post_pro)
                property_list.append(property_crop)

        crop_list = np.asarray(crop_list, dtype=object)
        mask_list = np.asarray(mask_list, dtype=object)
        property_list = np.asarray(property_list, dtype=object)
        return crop_list.reshape(grid.shape[0],grid.shape[1]), mask_list.reshape(grid.shape[0],grid.shape[1],mask.shape[0],mask.shape[1]), np.asarray(property_list).reshape(grid.shape[0],grid.shape[1])

    
    def _calculate_properties(self,mask):
        properties = {"num_instances": None, "area": None, "axis_major_length": None, "axis_minor_length": None,}
        mask = measure.label(mask)
        regions =  sorted(measure.regionprops(mask), key=lambda r: r.area, reverse=True)
        properties = dict()
        properties["num_instances"] = len(regions)
        if properties["num_instances"] > 0:
            for props in regions:
                properties["area"] = props.area
                properties["axis_major_length"] = props.axis_major_length
                properties["axis_minor_length"] = props.axis_minor_length
                mask[mask!=props.label] = 0
                break
        else:
            properties["area"], properties["axis_major_length"], properties["axis_minor_length"] = -1, -1, -1
        
        return properties, mask

    def export_results(self,processing_dir,results, bf_img,params,fl_img=None):
        grid    = params.get_grid()
        angle   = params.get_angle()

        bf_img=rotate(bf_img,angle,resize=True, preserve_range=True, order=0).astype("uint8")

        patch_dir = os.path.join(processing_dir, "patches")
        if not os.path.exists(patch_dir):
            os.makedirs(patch_dir)

        for i_r in range(grid.shape[0]):
            for i_c in range(grid.shape[1]):
                crop = results["crops"][i_r,i_c]
                mask = results["masks"][i_r,i_c]
    
                overlay = (label2rgb(mask, bg_label=0, 
                    image=cv2.resize(normalize(crop),(mask.shape[1],mask.shape[0])), 
                    image_alpha=0.7)*255).astype("uint8")

                tifffile.imwrite(os.path.join(patch_dir, f"spot_row_{str(i_r).zfill(2)}_col{str(i_c).zfill(2)}.tif"), overlay)

        property_lst = ["area", "axis_major_length", "axis_minor_length"]

        for prop in property_lst:
            info = f"spheroid_analysis, property {prop} (row,column) \n" \
                f"contact: marcel.schilling@kit.edu (IAI)"
            output_dec_format = "%." + str(1) + "f"
            file_name = os.path.join(processing_dir, f"spheroid_{prop}.txt")
            extracted_list = np.array([[results["properties"][i_r,i_c][prop] for i_c in range(grid.shape[1])] for i_r in range(grid.shape[0])])
            np.savetxt(file_name, extracted_list, fmt=output_dec_format, delimiter=' ', newline='\n', header=info, footer='',
                   comments='# ')
