import albumentations as A
import matplotlib.pyplot as plt
import onnxruntime as ort
import tifffile
from albumentations.pytorch.transforms import ToTensorV2
from skimage.color import label2rgb

from GS.processing.spheroid_detector_utils.distmap2inst import DistMapPostProcessor
from GS.processing.spheroid_detector_utils.norm_min_max import norm_min_max


def trafo(img):
    img = A.ToFloat(max_value=255.0).apply(img)
    img = norm_min_max(img)
    img = A.Resize(128,128).apply(img)
    return ToTensorV2().apply(img)

providers = [
    ('CUDAExecutionProvider', {
        'device_id': 0,
        'arena_extend_strategy': 'kNextPowerOfTwo',
        'gpu_mem_limit': 2 * 1024 * 1024 * 1024,
        'cudnn_conv_algo_search': 'EXHAUSTIVE',
        'do_copy_in_default_stream': True,
    }),
    'CPUExecutionProvider',
]

class UnetSpheroidDetectionPipeline:
    def __init__(self, 
            model_path) -> None:

        self.session = None
        self.dist_map_post_pro = None
        self._load_model(model_path)
        self._load_inst_post_pro()

    def process(self, img):
        img_pre_pro = self._pre_pro(img)
        pred_raw = self._inference(img_pre_pro)
        pred_raw = A.Resize(img.shape[0], img.shape[1]).apply(pred_raw)
        pred_inst = self._post_pro(pred_raw, img)
        return pred_inst

    def _inference(self, img_pre):
        output = self.session.run(
            None,
            {"input_1": img_pre.unsqueeze(0).numpy()},
        )
        return output[0].squeeze()

    def _pre_pro(self, img):
        img_pre = trafo(img)
        return img_pre

    def _post_pro(self, pred_inst, img): 
        pred_inst = self.dist_map_post_pro.process(pred_inst, img)
        return pred_inst

    def _load_model(self, model_path):
        self.session = ort.InferenceSession(model_path, providers=providers)
    
    def _load_inst_post_pro(self,):
        param_dict = dict()
        param_dict["sigma_cell"]                = 1.0
        param_dict["th_cell"]                   = 0.15
        param_dict["th_seed"]                   = 0.55
        param_dict["do_splitting"]              = True
        param_dict["do_area_based_filtering"]   = True
        param_dict["do_fill_holes"]             = True
        param_dict["valid_area_median_factors"] = [.25,3]
        self.dist_map_post_pro = DistMapPostProcessor(**param_dict)

if __name__ == "__main__":
    img_path = "/home/ws/sc1357/data/2022_DMA_Spheroid_Detection_split/unlabeled/samples/spot_row_44_col10.tif"
    img = tifffile.imread(img_path)[:,:,0]

    obj = UnetSpheroidDetectionPipeline("/home/ws/sc1357/data/trained_ann/2022_03_29_DMA_Spheroid_BF/dma_spheroid_bf.onnx")

    result = obj.process(img)

    plt.imshow((label2rgb(result, image=img, alpha=0.5)*255).astype("uint8"))
    plt.show()