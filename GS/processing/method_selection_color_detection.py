from PyQt5 import QtWidgets, uic
from PyQt5.QtGui import *
import os

import numpy as np

class MethodSelection(QtWidgets.QDialog):
    def __init__(self):
        super(MethodSelection, self).__init__()
        currFilepath = os.path.dirname(os.path.realpath(__file__))
        path = currFilepath + os.path.sep + 'method_selection_color_detection.ui'
        uic.loadUi(path, self)

        self.c1 = None
        self.c2 = None
        self.method = None

        

        self.methods.addItems(['HSV', 'RGB'])
        self.hsv_lst.addItems(['Hue', 'Saturation', 'Value'])
        self.color1.setStyleSheet("background-color: rgb(" + str(self.r1.value()) + "," + str(self.g1.value()) + "," + str(self.b1.value()) + ");")
        self.color2.setStyleSheet("background-color: rgb(" + str(self.r2.value()) + "," + str(self.g2.value()) + "," + str(self.b2.value()) + ");")

        self.r1.valueChanged.connect(self.change_c1_color)
        self.g1.valueChanged.connect(self.change_c1_color)
        self.b1.valueChanged.connect(self.change_c1_color)

        self.r2.valueChanged.connect(self.change_c2_color)
        self.g2.valueChanged.connect(self.change_c2_color)
        self.b2.valueChanged.connect(self.change_c2_color)

        self.base_r.valueChanged.connect(self.change_base_color)
        self.base_g.valueChanged.connect(self.change_base_color)
        self.base_b.valueChanged.connect(self.change_base_color)

        self.methods.currentTextChanged.connect(self.change_method)

        self.hsv_lst.currentTextChanged.connect(self.change_hsv)
        self.hsv_channel = self.hsv_lst.currentText()

        self.hide_color_selection()
        self.show_hsv_selection()


    def save_colors(self):
        if self.methods.currentText() == 'HSV':
            self.hsv_channel = self.hsv_lst.currentText()
        else:
            self.c1 = np.array([self.r1.value(), self.g1.value(), self.b1.value()])
            self.c2 = np.array([self.r2.value(), self.g2.value(), self.b2.value()])
            self.base_color = np.array([self.base_r.value(), self.base_g.value(), self.base_b.value()])

        self.method = self.methods.currentText()
        self.close()

    def change_base_color(self):
        self.base_color.setStyleSheet("background-color: rgb(" + str(self.base_r.value()) + "," + str(self.base_g.value()) + "," + str(self.base_b.value()) + ");")

    def change_c1_color(self):
        self.color1.setStyleSheet("background-color: rgb(" + str(self.r1.value()) + "," + str(self.g1.value()) + "," + str(self.b1.value()) + ");")

    def change_c2_color(self):
        self.color2.setStyleSheet("background-color: rgb(" + str(self.r1.value()) + "," + str(self.g1.value()) + "," + str(self.b1.value()) + ");")

    def change_method(self):
        if self.methods.currentText() == 'HSV':
            self.show_hsv_selection()
            self.hide_color_selection()
        else:
            self.hide_hsv_selection()
            self.show_color_selection()

    def change_hsv(self):
        self.hsv_channel = self.hsv_lst.currentText()

    def hide_hsv_selection(self):
        self.hsv_names.hide()
        self.hsv_lst.hide()

    def show_hsv_selection(self):
        self.hsv_names.show()
        self.hsv_lst.show()

    def hide_color_selection(self):
        self.label.hide()
        self.label_2.hide()
        self.label_3.hide()
        self.label_4.hide()
        self.label_5.hide()
        self.r1.hide()
        self.r2.hide()
        self.base_r.hide()
        self.g1.hide()
        self.g2.hide()
        self.base_g.hide()
        self.b1.hide()
        self.b2.hide()
        self.base_b.hide()
        self.color1.hide()
        self.color2.hide()
        self.base_color.hide()
        self.label_7.hide()

    def show_color_selection(self):
        self.label.show()
        self.label_2.show()
        self.label_3.show()
        self.label_4.show()
        self.label_5.show()
        self.r1.show()
        self.r2.show()
        self.base_r.show()
        self.g1.show()
        self.g2.show()
        self.base_g.show()
        self.b1.show()
        self.b2.show()
        self.base_b.show()
        self.color1.show()
        self.color2.show()
        self.base_color.show()
        self.label_7.show()