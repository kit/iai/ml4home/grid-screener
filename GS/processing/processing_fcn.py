from abc import ABC, abstractmethod


class ProcessingFcn(ABC):
    def __init__(self):
        super().__init__()


    @abstractmethod
    def run_process(self,bf_img,params,fl_img=None,*args): #c1 = None, c2 = None, base_color= None):
        raise NotImplementedError("Process fcn needs to be implemented")

    @abstractmethod
    def export_results(self, processing_dir,results, bf_img,params,fl_img=None, *args): # results_2 = None):
        raise NotImplementedError("Process fcn needs to be implemented")