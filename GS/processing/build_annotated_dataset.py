import slidingwindow as sw
import numpy as np
import os
import shutil
from skimage import img_as_ubyte
import matplotlib.pyplot as plt
from skimage.draw import disk, rectangle
from PIL import Image
from skimage.transform import rotate
import cv2
import json
import inspect

from GS.processing.processing_fcn import ProcessingFcn
from GS.grid_estimators.manual_detector import ManualDetector

class BuildAnnotatedDataset(ProcessingFcn):
    """
    Class for building an annotated dataset from a brightfield image using grid parameters.

    Attributes:
        train_foldername (str): The name of the training data folder.
        test_foldername (str): The name of the testing data folder.
        images_foldername (str): The name of the folder for sample images.
        masks_foldername (str): The name of the folder for labeled masks.
    """

    def __init__(self):
        super(BuildAnnotatedDataset,self).__init__()
        self.train_foldername="train"
        self.test_foldername="test"
        self.images_foldername="samples"
        self.masks_foldername="labels"


    def run_process(self,bf_img,params,px_win_size=200,split_ratio=0.2):
        """
        Run a processing pipeline on the given brightfield image to create an annotated dataset using specified parameters.

        Args:
            bf_img (numpy.ndarray): The input brightfield image to process.
            params (object): An object containing grid parameters, including an angle.
            px_win_size (int): The window size for pixel extraction. Default is 200.
            split_ratio (float): The ratio to split the dataset into training and testing sets.
                                Default is 0.2.

        Returns:
            dict: A dictionary containing the results of the processing pipeline.

        Raises:
            cv2.error: If there is an OpenCV error while rotating the image.

        Note:
            This method performs image rotation, creates a mask based on grid parameters, and generates an annotated dataset.
        """

        angle=params.get_angle()
        try:
            img=ManualDetector.rotate(self,image=bf_img,angle=angle).astype("int")
        except cv2.error:
            img=rotate(bf_img,angle,resize=True, preserve_range=True, order=0).astype("int")
        img_mask=self.grid_params_to_mask(img,params)
        results=self.create_annotated_dataset(img,img_mask,px_win_size,split_ratio)
        return results

    def grid_params_to_mask(self,img,params):
        """
        Create a binary mask based on grid parameters and the shape specified in the parameters.

        Args:
            img (numpy.ndarray): The input image on which to create the mask.
            params (object): An object containing grid and shape parameters.

        Returns:
            numpy.ndarray: A binary mask where selected regions are set to 1, and the rest are 0.

        Note:
            This method creates a binary mask based on the grid parameters provided. The mask shape can be either square or circle,
            determined by the 'shape' parameter in the 'params' object. For each grid point, a square or circular region is filled
            with 1 in the mask.
        """

        d = params.get_diameter()
        grid = params.get_grid()
        shape = params.get_shape()
        bf_img=img
        mask = np.zeros(bf_img.shape[0:2])

        for row in grid:
            for point in row:
                x_i = int(point[0])
                y_i = int(point[1])
                if shape == "square":
                    rect = plt.Rectangle((x_i - d / 2, y_i - d / 2), d, d, facecolor='none', linewidth=0.1,
                    edgecolor='c', )
                    rect_borders = plt.Rectangle((x_i - d / 2, y_i - d / 2), d, d, facecolor='none', linewidth=0.2,
                    edgecolor='r', )
                    start = (int(y_i - d / 2), int(x_i - d / 2))
                    extent = (int(d), int(d))
                    yy, xx = rectangle(start=start, extent=extent, shape=bf_img.shape[0:2])
                    mask[yy, xx] = 1
                elif shape == "circle":
                    circ = plt.Circle((x_i, y_i), d / 2, fill=False, linewidth=0.1, color='c', )
                    circ_borders = plt.Circle((x_i, y_i), d / 2, fill=False, linewidth=0.2, color='r', )
                    yy, xx = disk((y_i, x_i), radius=d / 2, shape=bf_img.shape[0:2])
                    mask[yy, xx] = 1

        mask=img_as_ubyte(mask)
        return mask


    def create_annotated_dataset(self,img,img_mask,px_win_size,split_ratio):
        """
        Create an annotated dataset by splitting the input image and mask into training and testing sets.

        Args:
            img (numpy.ndarray): The input image to create the dataset from.
            img_mask (numpy.ndarray): The binary mask corresponding to the input image.
            px_win_size (int): The window size for pixel extraction.
            split_ratio (float): The ratio to split the dataset into training and testing sets.

        Returns:
            list: A list containing four lists with the extracted images: img_list_train, img_list_test, mask_list_train, mask_list_test.

        Note:
            This method generates an annotated dataset by splitting the input image and its corresponding mask into
            training and testing sets based on the provided split ratio. It uses sliding windows to extract patches
            from the image and mask and assigns them to the appropriate set.
        """

        windows = sw.generate(img, sw.DimOrder.HeightWidthChannel, px_win_size, 0.5)
        img_list_train=list()
        mask_list_train=list()
        img_list_test=list()
        mask_list_test=list()

        for window in windows:
            split=[self.test_foldername,self.train_foldername]
            choice=np.random.choice(split,1,p=[split_ratio,1-split_ratio])[0]
            image_crop = img[window.indices()]
            mask_crop = img_mask[window.indices()]
            if choice==self.train_foldername:
                img_list_train.append(image_crop)
                mask_list_train.append(mask_crop)
            else:
                img_list_test.append(image_crop)
                mask_list_test.append(mask_crop)
        return [img_list_train,img_list_test,mask_list_train,mask_list_test]
            

    def build_directory_structure(self,processing_dir):
        """
        Build the directory structure for the annotated dataset processing project.

        Args:
            processing_dir (str): The path to the main processing directory.

        Returns:
            None

        Note:
            This method creates a directory structure for organizing an annotated dataset processing project. It creates
            folders for 'annotated_dataset', 'meta_info', 'temp', 'train_cfg', 'unlabeled', that can later be used as the
            project directory folders of a kaida project and subfolders for training and testing sets within the 
            'annotated_dataset' directory. It also prepares project files as needed.
        """

        os.chdir(processing_dir)
        path="annotated_dataset"
        self.create_folder(path)

        for folder in (self.test_foldername,self.train_foldername):
            os.chdir(path)
            self.create_folder(folder)
            self.create_folder("meta_info")
            self.create_folder("temp")
            self.create_folder("train_cfg")
            self.create_folder("unlabeled")
            self.prepare_project_file()
            os.chdir(folder)
            self.create_folder(self.masks_foldername)
            self.create_folder(self.images_foldername)
            os.chdir(processing_dir)


    def create_folder(self,foldername):
        """
        Create a folder with the specified name or replace it if it already exists.

        Args:
            foldername (str): The name of the folder to create.

        Returns:
            None
        """

        if os.path.exists(foldername):
            shutil.rmtree(foldername)
        os.makedirs(foldername)

    def prepare_project_file(self):
        """
        Prepare and update the project configuration file for the processing project.

        Args:
            None

        Returns:
            None

        Note:
            This method prepares and updates the project configuration file by copying the default 'kaida_project.json' file
            and making necessary modifications to it. It sets the project file name and data directory paths accordingly.
        """

        source_directory = os.path.dirname(os.path.abspath(inspect.getframeinfo(inspect.currentframe()).filename))
        json_file_name = 'kaida_project.json'
        shutil.copy(os.path.join(source_directory, json_file_name), json_file_name)
        json_path = os.path.join(os.getcwd(), json_file_name)
        with open(json_path, 'r') as json_file:
            data = json.load(json_file)
            data['prj_file_name'] = os.path.join(os.getcwd(),data["name"])
            data['data_dir'] = os.getcwd()
        with open(json_path, 'w') as json_file:
            json.dump(data, json_file, indent=4)


    
    def export_results(self,processing_dir,results, px_win_size,bf_img,params=None,fl_img=None):
        """
        Export processing results, including images, masks and metadata, to the specified directory.

        Args:
            processing_dir (str): The directory where the results should be exported.
            results (list): A list containing processed images and masks.
            px_win_size (int): The window size for pixel extraction.
            bf_img (numpy.ndarray): The input brightfield image used in processing.
            params (object): An object containing processing parameters. Default is None.
            fl_img (numpy.ndarray): An optional fluorescence image. Default is None.

        Returns:
            None

        Note:
            This method exports the processing results to the specified directory, including processed images and masks.
            It also generates and exports metadata files such as parameters in JSON format, that are necessary for a further training using kaida.
        """

        self.build_directory_structure(processing_dir)
        choices=[self.train_foldername,self.test_foldername,self.train_foldername,self.test_foldername]
        types=[self.images_foldername,self.images_foldername,self.masks_foldername,self.masks_foldername]

        for i in range(len(results)):
            iter=0
            start_index=len(results[0])
            for tif in results[i]:
                if choices[i]==self.train_foldername and types[i]==self.images_foldername:
                    path=os.path.join(os.path.join(os.path.join(os.path.join(processing_dir,"annotated_dataset"),choices[i]),types[i]),str(iter)+".tif")
                    tif=np.uint8(tif)
                elif choices[i]==self.test_foldername and types[i]==self.images_foldername:
                    path=os.path.join(os.path.join(os.path.join(os.path.join(processing_dir,"annotated_dataset"),choices[i]),types[i]),str(iter+start_index)+".tif")
                    tif = np.uint8(tif)
                elif choices[i]==self.train_foldername and types[i]==self.masks_foldername:
                    path=os.path.join(os.path.join(os.path.join(os.path.join(processing_dir,"annotated_dataset"),choices[i]),types[i]),str(iter)+"_label.tif")
                    tif=np.uint8(tif)
                elif choices[i]==self.test_foldername and types[i]==self.masks_foldername:
                    path=os.path.join(os.path.join(os.path.join(os.path.join(processing_dir,"annotated_dataset"),choices[i]),types[i]),str(iter+start_index)+"_label.tif")
                    tif = np.uint8(tif)
                
                Image.fromarray(tif).save(path)
                iter+=1   

        diameter=params.get_diameter()
        scale_factor=float(200)/float(px_win_size)
        origin="GS-kaida-GS"

        parameters = {
            "grid_detection.parameters.diameter_size": diameter,
            "grid_detection.parameters.scale_factor": scale_factor,
            "grid_detection.parameters.origin": origin
        }
        
        json_path = os.path.join(os.path.join(processing_dir,"annotated_dataset"), "GS_params_for_kaida.json")
        with open(json_path, "w") as json_file:
            json.dump(parameters, json_file)