from cmath import pi
import cv2
import os
import pandas as pd
import numpy as np
from skimage.transform import rotate
from skimage.draw import disk
import matplotlib.pyplot as plt
import tifffile

from GS.processing.processing_fcn import ProcessingFcn

class ColorEvaluation(ProcessingFcn):
    def __init__(self):
        super(ColorEvaluation,self).__init__()

    def run_process(self,bf_img,params,fl_img, c1 = None, c2 = None, base_color = None, channel=None):
        angle= params.get_angle()
        self.diameter = params.get_diameter()

        bf_img = rotate(bf_img,angle,resize=True, preserve_range=True, order=0).astype("uint8")

        if c1 is not None :
            results_c1, results_c2 = self._color_intensity_per_spot_RGB(bf_img, params, c1, c2, base_color)
            return results_c1, results_c2
        else:
            results = self._color_intensity_per_spot_HSV(bf_img, params, channel)
            return results

    def _color_intensity_per_spot_HSV(self, bf_img, params, hsv_channel):
        grid    = params.get_grid()

        intensity_per_spot = np.zeros((grid.shape[0],grid.shape[1]))

        if hsv_channel=="Hue":
            c_ind = 0
        elif hsv_channel=="Saturation":
            c_ind = 1
        else:
            c_ind = 2

        hsv = cv2.cvtColor(bf_img, cv2.COLOR_RGB2HSV)

        for i_r in range(grid.shape[0]):
            for i_c in range(grid.shape[1]):
                mask_i = np.zeros(bf_img.shape[0:2], dtype=bool)
                x_i = grid[i_r,i_c,0]
                y_i = grid[i_r,i_c,1]

                yy, xx = disk((y_i, x_i), radius= 4, shape=bf_img.shape[0:2])
                mask_i[yy, xx] = True

                #intensity_per_spot[i_r,i_c] = np.mean(hsv[mask_i,c_ind])

                if c_ind == 0:
                    intensity_per_spot[i_r,i_c] = np.mean(hsv[mask_i,c_ind])*2
                else:
                    intensity_per_spot[i_r,i_c] = np.mean(hsv[mask_i,c_ind])/255
                
        
        return intensity_per_spot

    def _color_intensity_per_spot_RGB(self, bf_img, params, c1, c2, base_color):
        grid    = params.get_grid()
        
        pink = c1
        blue = c2
        
        img_r = bf_img[:,:,0].copy()
        img_g = bf_img[:,:,1].copy()
        img_b = bf_img[:,:,2].copy()

        intensity_per_spot_blue = np.zeros((grid.shape[0],grid.shape[1]))
        intensity_per_spot_pink = np.zeros((grid.shape[0],grid.shape[1]))

        for i_r in range(grid.shape[0]):
            for i_c in range(grid.shape[1]):
                mask_i = np.zeros_like(img_r, dtype=bool)
                x_i = grid[i_r,i_c,0]
                y_i = grid[i_r,i_c,1]

                yy, xx = disk((y_i, x_i), radius= 4, shape=img_r.shape[0:2])
                mask_i[yy, xx] = True

                mean_r = np.mean(img_r[mask_i])
                mean_g = np.mean(img_g[mask_i])
                mean_b = np.mean(img_b[mask_i])
                mean = np.array([mean_r, mean_g, mean_b])

                intensity_per_spot_blue[i_r,i_c] = self.calc_intensity(blue, mean, base_color)
                intensity_per_spot_pink[i_r,i_c] = self.calc_intensity(pink, mean, base_color)

        return intensity_per_spot_pink, intensity_per_spot_blue

    def calc_intensity(self, color, mean, base_color):
        distance_total = np.sqrt((255-color[0])**2 + (255-color[1])**2 + (255-color[2])**2) 
        projection = base_color + (np.dot(mean-base_color, base_color-color)/ np.dot(base_color-color, base_color-color)) * (base_color-color) 
        distance_white_point_b = np.sqrt((base_color[0]-projection[0])**2 + (base_color[1]-projection[1])**2 + (base_color[2]-projection[2])**2) 
        intensity = distance_white_point_b / distance_total
        
        if intensity > 1:
            intensity_per_spot = 2 - intensity
        else:
            intensity_per_spot = intensity
        
        return intensity_per_spot

    def export_results(self, processing_dir, results_c1, bf_img, params, fl_img=None, results_c2= None, c1 = None, c2 = None):
        path = os.path.join(processing_dir, 'color_evaluation')
        if not os.path.exists(path):
            os.makedirs(path)
        
        if results_c2 is None:
            grid= params.get_grid()
            df = pd.DataFrame(results_c1)
            df.to_csv(os.path.join(path, 'results_saturation_HSV.csv'),index=False, header = False)

            self.show_plot(results_c1, grid, 'HSV saturations', bf_img, path)

        else:
            grid= params.get_grid()
            df = pd.DataFrame(results_c1)
            df.to_csv(os.path.join(path, 'results_saturation_color1_' + str(c1) + '.csv'),index=False, header = False)
            self.show_plot(results_c1, grid, 'Saturation of color 1 (RGB: ' + str(c1) + ')', bf_img, path)

            df = pd.DataFrame(results_c1)
            df.to_csv(os.path.join(path, 'results_saturation_color2_' + str(c2) + '.csv'),index=False, header = False)
            self.show_plot(results_c2, grid, 'Saturation of color 2 (RGB:  ' + str(c2) + ')', bf_img, path)
            
    
    def show_plot(self,result,grid, title, img, path):
        fig= plt.figure()
        ax=fig.add_subplot()
        
        fontsize = self.diameter / (1.5 * fig.dpi) #int(self.diameter/35)

        for i_r in range(grid.shape[0]):
            for i_c in range(grid.shape[1]):
                
                x_i = grid[i_r,i_c,0]
                y_i = grid[i_r,i_c,1]
                text = str(int(round(result[i_r, i_c],2)*100)) + '%'

                ax.text(int(x_i), int(y_i), text, ha='center', va='center', fontsize = fontsize)
        
        ax.imshow(img)
        plt.axis('off')        

        title_save = title.replace(' ', '_')
        path_new = os.path.join(path, title_save + '.tif')
        plt.savefig(os.path.join(path_new), dpi=1000)
        plt.clf()

        img_new = tifffile.imread(path_new)  #Funktioniert so, ist aber unnötig viel Arbeit
        plt.imshow(img_new)
        plt.axis('off')

        plt.title(title + (' (Percentage written in spot)'))
        plt.show()