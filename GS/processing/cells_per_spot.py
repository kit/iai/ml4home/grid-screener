import os

import cv2
import matplotlib.pyplot as plt
import numpy as np
import tifffile
from skimage import measure
from skimage.draw import disk, rectangle
from skimage.transform import rotate

from GS.processing.processing_fcn import ProcessingFcn

def normalize(img):
    return (img - img.min()) / (img.max() - img.min())

class CellsPerSpot(ProcessingFcn):
    def __init__(
        self,
        threshold_step,
        percentile,
        threshold_cc,
        filter_area,
        min_area,
        max_area,
        min_repeatability,
        min_dist_blobs,
        filter_circularity,
        min_circularity,
        filter_inertia,
        min_inertia,
        filter_convexity,
        min_convexity
    ):
        super(CellsPerSpot,self).__init__()
        self.threshold_step = threshold_step
        self.percentile = percentile
        self.threshold_cc = threshold_cc
        self.filter_area = filter_area
        self.min_area = min_area
        self.max_area = max_area
        self.min_repeatability = min_repeatability
        self.min_dist_blobs = min_dist_blobs
        self.filter_circularity = filter_circularity
        self.min_circularity =  min_circularity
        self.filter_inertia = filter_inertia
        self.min_inertia = min_inertia
        self.filter_convexity = filter_convexity
        self.min_convexity = min_convexity
        
    
    def run_process(self,bf_img,params,fl_img=None):
        angle   = params.get_angle()

        fl_img = rotate(fl_img,angle,resize=True, preserve_range=True, order=0).astype(fl_img.dtype)

        cell_predictions = self._predict_cells(fl_img)

        cells_per_spot,crop   = self._count_cells_per_spot(fl_img,cell_predictions, params)

        return {"predictions": cell_predictions, "counted": cells_per_spot, "crop" : crop}


    def _predict_cells(self, fl_img):
         # extract only green channel
        #fl_img = fl_img[:, :, 1]

        fl_img = (normalize(fl_img)*255).astype("uint8")

        blob_detection = self._blob_detection(fl_img)

        detection_filtered = self._segmentation_filter(blob_detection,fl_img )
       
        return detection_filtered

    def _segmentation_filter(self,blob_detection, fl_img):
        filter_threshold = np.percentile(fl_img, 98, interpolation='linear')

        segment = measure.label((fl_img>filter_threshold).astype("uint8"))

        r_props = measure.regionprops(segment)

        for prop in r_props:
            if np.sum(blob_detection[segment==prop.label])>5:
                blob_detection[segment==prop.label] = 0

        return blob_detection

    def _blob_detection(self, fl_img):
        # init param obj and assign params
        params_blob = cv2.SimpleBlobDetector_Params()
        params_blob.filterByColor = False

        params_blob.thresholdStep = self.threshold_step
        params_blob.minThreshold = np.percentile(fl_img, self.percentile, interpolation='linear')

        params_blob.maxThreshold = 255

        params_blob.filterByArea = self.filter_area
        params_blob.minArea = self.min_area
        params_blob.maxArea = self.max_area

        params_blob.minRepeatability = self.min_repeatability
        params_blob.minDistBetweenBlobs = self.min_dist_blobs

        params_blob.filterByCircularity = self.filter_circularity
        params_blob.minCircularity = self.min_circularity

        params_blob.filterByInertia = self.filter_inertia
        params_blob.minInertiaRatio = self.min_inertia

        params_blob.filterByConvexity = self.filter_convexity
        params_blob.minConvexity = self.min_convexity

        ver = cv2.__version__.split('.')
        if int(ver[0]) < 3:
            detector_blob = cv2.SimpleBlobDetector(params_blob)
        else:
            detector_blob = cv2.SimpleBlobDetector_create(params_blob)

        detector_blob.empty()

        kp_blob = detector_blob.detect(fl_img)

        detection = np.zeros_like(fl_img)

        for kp in kp_blob:
            detection[int(kp.pt[1]), int(kp.pt[0])] = 1

        return detection


    def _count_cells_per_spot(self, fl_img, cell_predictions, params):
        d       = params.get_diameter()
        grid    = params.get_grid()
        shape   = params.get_shape()

        cells_per_spot = np.zeros((grid.shape[0],grid.shape[1]))
        crop_list=list()

        for i_r in range(grid.shape[0]):
            for i_c in range(grid.shape[1]):
                mask_i = np.zeros_like(cell_predictions, dtype=bool)
                x_i = grid[i_r,i_c,0]
                y_i = grid[i_r,i_c,1]
                if shape == "square":
                    start = (int(y_i - d / 2), int(x_i - d / 2))
                    extent = (int(d), int(d))
                    yy, xx = rectangle(start=start, extent=extent, shape=cell_predictions.shape[0:2])
                    mask_i[yy, xx] = True
                elif shape == "circle":
                    yy, xx = disk((y_i, x_i), radius=d / 2, shape=cell_predictions.shape[0:2])
                    mask_i[yy, xx] = True

                cells_per_spot[i_r,i_c] = np.sum(cell_predictions[mask_i])

                relevant_detections = cell_predictions*mask_i
                crop = fl_img[int(y_i - d / 2):int(y_i + d / 2), int(x_i - d / 2):int(x_i + d / 2)].copy()
                crop_detect = relevant_detections[int(y_i - d / 2):int(y_i + d / 2), int(x_i - d / 2):int(x_i + d / 2)].copy()

                detect_arg = np.argwhere(crop_detect==1)

                for p in detect_arg:
                    crop = cv2.drawMarker(crop, (int(p[1]),int(p[0])), color=(255,0,0), markerType=cv2.MARKER_CROSS, thickness=1, markerSize=5)
                crop_list.append(crop)
        crop_list=np.asarray(crop_list)
        return cells_per_spot,crop_list.reshape(grid.shape[0],grid.shape[1])


    def export_results(self,processing_dir,results, bf_img,params,fl_img=None):
        d       = params.get_diameter()
        grid    = params.get_grid()
        shape   = params.get_shape()
        angle   = params.get_angle()

        cells_per_spot = results["counted"]

        fl_img=rotate(fl_img,angle,resize=True, preserve_range=True, order=0).astype("uint8")

        patch_dir = os.path.join(processing_dir, "patches")
        
        if not os.path.exists(patch_dir):
            os.makedirs(patch_dir)

        org = (0, 12)
        font = cv2.FONT_HERSHEY_SIMPLEX
        font_scale = 0.4
        for i_r in range(grid.shape[0]):
            for i_c in range(grid.shape[1]):
                crop=results["crop"][i_r,i_c]

                text = f"r: {i_r}, c: {i_c}, num: {cells_per_spot[i_r,i_c]}"
                crop = cv2.putText(crop, text, org, font, font_scale, color=(255,0,0))
                
                tifffile.imwrite(os.path.join(patch_dir, f"spot_row_{str(i_r).zfill(2)}_col{str(i_c).zfill(2)}.tif"), crop)
        
        info = f"cells per spot (row,column) \n" \
               f"contact: marcel.schilling@kit.edu (IAI)"
        output_dec_format = "%." + str(1) + "f"
        file_name = os.path.join(processing_dir, f"cells_per_spot.txt")
        np.savetxt(file_name, cells_per_spot, fmt=output_dec_format, delimiter=' ', newline='\n', header=info, footer='',
                   comments='# ')

        fig, ax = plt.subplots()
        title = f"Cells per spots"
        fig.suptitle(title)
        cps_plt = ax.imshow(cells_per_spot.astype("uint8"), cmap="gray")
        cbar = fig.colorbar(cps_plt, extend='both', shrink=0.9, ax=ax)
        cbar.set_label('#num cells per spot')
        ax.set_xlabel("columns")
        ax.set_ylabel("rows")
        fig.tight_layout()
        fig_name = os.path.join(processing_dir, f"plt_cells_per_spot.png")
        plt.savefig(fig_name, dpi=300)
        plt.close()