from re import I
from PyQt5 import QtCore, QtGui, QtWidgets, uic
import os
import tifffile
import cv2
import matplotlib.pyplot as plt
import matplotlib.widgets as wdg
import pickle
import numpy as np
import math
import yaml
from skimage.transform import rotate, rescale
from skimage.draw import disk, rectangle
import json
import copy
import tarfile
import shutil
import wandb
from PyQt5 import QtCore, QtGui, QtWidgets, uic


from GS.utils.grid_parameter import GridParameter
from GS.utils.mapping import *
from GS.dnn.dl_processor import DnnProcessor
from GS.utils.config_parser import ConfigParser
from GS.utils.helper_gui.parameter_table import ParameterTable
from GS.processing.method_selection_color_detection import MethodSelection
from GS.utils.param_handling import split_parameters
from GS.grid_estimators.estimated_grid_figure import EstimatedGridDialog



class GridScreenerGui(QtWidgets.QMainWindow):
    def __init__(self):
        super(GridScreenerGui, self).__init__()
        currFilepath = os.path.dirname(os.path.realpath(__file__))
        uiFile=currFilepath + os.path.sep + 'gui_grid_screener.ui'
        uic.loadUi(uiFile, self)

        self.bf_img = None
        self.fl_img = None
        self.result_dir = None
        self.cfg = dict()

        self.color.hide()

        self.detector_comboBox.currentIndexChanged.connect(self.change_visibility)
        self.manuel_grid_group.setVisible(False)

        self.initation = True
        # init table widget
        self.grid_details_tbl.horizontalHeader().hide()
        self.update_table()
        self.num_grid_groups_lineedit.editingFinished.connect(self.update_table)
        
        # init detector comboBox
        self.detector_comboBox.addItems(get_known_detectors().keys())

        # init process comboBox
        self.process_comboBox.blockSignals(True)
        self.process_comboBox.addItem("None")
        self.process_comboBox.addItems(get_known_processing_fcns().keys())
        self.process_comboBox.blockSignals(False)
        self.cfg["processing_fcn"]=dict()
        self.cfg["processing_fcn"]["selected"] = self.process_comboBox.currentText()

        # init dnn predictor
        self.dnn_predictor      = None

        self.ds_spinbox.valueChanged.connect(self.update_scale_factor)

        self.showPlot.setChecked(True)
        self.saveProcessing.setChecked(True)

        if os.path.isfile("default_scanner_cfg.yaml"):
            with open("default_scanner_cfg.yaml","r") as file:
                default_cfg_dict = yaml.safe_load(file, )
            self.config_file_path   = default_cfg_dict["config_file_path"]
            self.weight_file_path   = default_cfg_dict["weight_file_path"]
        else:
            self.config_file_path   = None
            self.weight_file_path   = None

        self.tar_file_path=None
        self.tar_dir="tar_dir"

        self.initiation = False

        self.process_comboBox.currentTextChanged.connect(self.change_process)

    def closeEvent(self, event):
        """
        Override the close event handler to remove a temporary directory if it exists.

        Args:
            self (object): The instance of the class.
            event: The close event.

        Returns:
            None

        Note:
            This method overrides the default close event handler to perform additional actions before closing the application.
            If the specified temporary directory (`self.tar_dir`) exists, it is removed using `shutil.rmtree` to clean up resources.
            The method then calls the parent class's close event handler to continue the closing process.
        """

        if os.path.exists(self.tar_dir):
            shutil.rmtree(self.tar_dir)
        super().closeEvent(event)

    def change_process(self):
        if self.process_comboBox.currentText() == 'color_evaluation':
            self.color.show()
            self.color_selection()
        else:
            self.color.hide()

    def load_bf_image(self):
        """
        Load a brightfield image file using a file dialog.

        Args:
            None

        Returns:
            None

        Note:
            This method opens a file dialog to allow the user to select a brightfield image file.
            It updates the GUI with the selected file name and initializes relevant attributes.
            If the selected file type is '.tif', it uses tifffile to read the image; otherwise, it uses OpenCV.
            The method also creates a results directory based on the selected file name if it doesn't exist.
        """
         
        self.bf_file_path=QtWidgets.QFileDialog.getOpenFileName(self)[0].replace('/',os.sep)
        try:
            filename=self.bf_file_path.split(os.sep)[-1]
            self.bf_lbl.setText(filename)
            self.img_type = self.bf_file_path.split(".")[1]
            self.result_dir = os.path.join(
                os.path.dirname(self.bf_file_path), 
                f"results_{os.path.splitext(self.bf_file_path.split(os.sep)[-1])[0]}"
            )

            if not os.path.exists(self.result_dir):
                os.makedirs(self.result_dir)

            self.bf_img = tifffile.imread(self.bf_file_path) if self.img_type == "tif" else cv2.imread(self.bf_file_path,-1)

        except:
            return

    def load_fl_image(self):
        """
        Load a fluorescence image file using a file dialog.

        Args:
            None

        Returns:
            None

        Note:
            This method opens a file dialog to allow the user to select a fluorescence image file.
            It updates the GUI with the selected file name and initializes relevant attributes.
            If the selected file type is '.tif', it uses tifffile to read the image; otherwise, it uses OpenCV.
        """

        self.fl_file_path=QtWidgets.QFileDialog.getOpenFileName(self,"Select file")[0].replace('/',os.sep)
        try:    
            self.fl_img = tifffile.imread(self.fl_file_path) if self.img_type == "tif" else cv2.imread(self.fl_file_path,-1)
            filename=self.fl_file_path.split(os.sep)[-1]
            self.fl_lbl.setText(filename)
            self.fl_img_type = self.fl_file_path.split(".")[1]
            
        except:
            return
        
    def load_tar(self):
        """
        Load a DNN tar file using a file dialog.

        Args:
            None

        Returns:
            None

        Note:
            This method opens a file dialog to allow the user to select a DNN tar file.
            It updates the GUI with the selected file name and initializes relevant attributes.
            If the selected tar file exists, it extracts its contents into a specified directory (`self.tar_dir`).
            The method then reads configuration and weight files from the extracted contents.
            The scale factor, diameter size, and origin are read from the configuration file and updated in the GUI.
        """
        
        self.tar_file_path = QtWidgets.QFileDialog.getOpenFileName(self, "Select DNN tar file")[0]
        try:
            filename = self.tar_file_path.split("/")[-1]
            self.tar_lbl.setText(filename)

            if not os.path.exists(self.tar_dir):
                os.makedirs(self.tar_dir)
            with tarfile.open(self.tar_file_path, "r") as tar:
                tar.extractall(self.tar_dir)
                extracted_files = []
                for tarinfo in tar:
                    if tarinfo.name.startswith("./"):
                        extracted_files.append(os.path.join(self.tar_dir, tarinfo.name[2:]))
            self.config_file_path = extracted_files[0]
            self.weight_file_path = extracted_files[1]

            self.scale_factor, self.diameter_size, self.origin = self.read_scale_factor(self.config_file_path)
            self.org_diameter_size = copy.deepcopy(self.diameter_size * math.sqrt(self.scale_factor / 100))
            self.sf_spinbox.setValue(self.scale_factor)
            self.ds_spinbox.setValue(self.diameter_size)

        except:
            return

    def init_dnn_predictor(self):
        """
        Initialize a deep neural network (DNN) predictor using configuration and checkpoint files.

        Args:
            None

        Returns:
            None

        Note:
            This method initializes a DnnProcessor instance by providing the configuration and checkpoint file paths.
            It also updates the default configuration for the DNN predictor.
        """

        self.dnn_predictor = DnnProcessor(
            config_path=self.config_file_path, 
            checkpoint_path=self.weight_file_path
        )
        self.update_default_config()

    def update_default_config(self):
        """
        Update the default configuration file with the paths to the DNN configuration and weight files.

        Args:
            None

        Returns:
            None

        Note:
            This method creates a dictionary containing the paths to the configuration and weight files.
            It then writes this dictionary to a default configuration file named "default_scanner_cfg.yaml."
        """

        default_cfg_dict = dict()
        default_cfg_dict["config_file_path"] = self.config_file_path
        default_cfg_dict["weight_file_path"] = self.weight_file_path
        with open("default_scanner_cfg.yaml","w") as outfile:
            yaml.dump(default_cfg_dict, outfile,default_flow_style=False)


    def show_image(self):
        """
        Display the loaded brightfield image with optional resizing and color mapping.

        Args:
            None

        Returns:
            None

        Note:
            This method displays the loaded brightfield image with optional resizing if its dimensions are too large.
            It also applies color mapping based on the number of unique colors in the image, defaulting to 'gray' for grayscale images
            and 'viridis' for color images.
            The displayed image is shown using Matplotlib.
        """

        if self.result_dir is None:
            self.statusBar().showMessage("Select image first",5000)
            return

        height, width=self.bf_img.shape[:2]
        max_f=max(height, width)
        if max_f>10000:
            scale = 10000 / max_f
            resized_image = cv2.resize(self.bf_img, None, fx=scale, fy=scale, interpolation=cv2.INTER_AREA)
        else:
            resized_image=self.bf_img
        if len(resized_image.shape)<3:
            cmap="gray"
        else:
            gray = cv2.cvtColor(resized_image, cv2.COLOR_BGR2GRAY)
            _, binary = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
            unique_colors = np.unique(binary)
            cmap = 'gray' if len(unique_colors) <= 2 else 'viridis'
        plt.imshow(resized_image,cmap=cmap)
        plt.show()
        return
    

    def update_scale_factor(self):
        """
        Update the scale factor based on the diameter size entered in the UI.

        Args:
            None

        Returns:
            None

        Note:
            This method retrieves the diameter size from the UI spinbox and calculates the corresponding scale factor.
            The scale factor is then updated in the UI using the sf_spinbox.
        """    

        self.diameter_size=self.ds_spinbox.value()
        ratio=self.org_diameter_size/self.diameter_size
        self.scale_factor=ratio*ratio*100
        self.sf_spinbox.setValue(self.scale_factor)

    def read_scale_factor(self, config_path):
        """
        Read the scale factor, diameter size, and origin from a configuration file.

        Args:
            config_path (str): Path to the configuration file.

        Returns:
            int: The scale factor (multiplied by 100).
            float: The diameter size.
            str: The origin.
        """

        cfg_yaml = yaml.load(config_path, Loader=yaml.FullLoader)
        wandb.init(
            config=cfg_yaml,
            allow_val_change=True,
            project=None,
            mode="disabled",
        )
        config = wandb.config
        parameters_splitted = split_parameters(config, ["model", "data", "spot_detection","grid_detection"])
        spot_detection_params = split_parameters(parameters_splitted["grid_detection"])["parameters"]
        scale_factor   = spot_detection_params["scale_factor"]
        diameter_size  = spot_detection_params["diameter_size"]
        origin  = spot_detection_params["origin"]
        return int(scale_factor*100), diameter_size, origin

    
    def get_scale_factor(self):
        """
        Get the current scale factor value from the UI spinbox.

        Args:
            None

        Returns:
            int: The current scale factor value.
        """

        scale_factor=self.sf_spinbox.value()
        return scale_factor

    def start_grid_detection(self):
        """
        Start the grid detection process based on the selected detector and configuration.

        This method performs grid detection using either the "Automated" or "Semi-Automated" mode, as selected by the user.
        The grid detection result, including rotation angle, grid parameters, and shape, is saved to a file.

        Args:
            None

        Returns:
            None
        """

        if self.result_dir is None:
            self.statusBar().showMessage("Select image first",5000)
            return

        detector_name = self.detector_comboBox.currentText()

        self.statusBar().showMessage("Grid detection...")
        self.spinner_widget.start()
        self.repaint()

        if detector_name == "Automated":
            if self.tar_file_path is None or self.tar_file_path=="":
                self.statusBar().showMessage("Select tar file first",5000)
                self.spinner_widget.stop()
                self.repaint()
                return
            self.init_dnn_predictor()
            self.scale_factor=float(self.get_scale_factor()/100)
            mask = self.dnn_predictor.predict(self.bf_img,self.scale_factor,self.origin)

        detector = get_estimator(detector_name)
        if detector_name == "Automated":
            if self.acc_mode_checkbox.isChecked():
                resize_factor = 0.2
            else:
                resize_factor = 1
            try:
                rot_angle, grid_params, shape,d = detector.process(mask,resize_factor)
            except UnboundLocalError:
                QtWidgets.QMessageBox.critical(self, 'Error',"Image too small for \"Accelerated Mode\"!\nPlease turn it off and start grid detection again.")
                self.statusBar().showMessage("Accelerated Mode failed")
                self.spinner_widget.stop()
                self.repaint()
                return
        elif detector_name == "Semi-Automated":
            shape = self.shape_comboBox.currentText()
            grid_shape=self.get_grid_details()
            if grid_shape==None:
                self.statusBar().showMessage("Wrong input. Numeric values needed.")
                return
            try:
                rot_angle, grid_params, d = detector.process(self.bf_file_path, self.num_groups,grid_shape)
            except TypeError:
                self.statusBar().showMessage("Grid detection canceled")
                self.spinner_widget.stop()
                self.repaint()
                return
            except cv2.error:
                QtWidgets.QMessageBox.critical(self, 'Error',"Image too big!")
                self.statusBar().showMessage("Image too big")
                self.spinner_widget.stop()
                self.repaint()
                return
            
        params = GridParameter(
            rot_angle,
            grid_params,
            shape,
            d
        )

        sv = os.path.join(self.result_dir, "grid.params")
        pickle.dump(params,open(sv,"wb"))


        detection_dir = os.path.join(self.result_dir, "detections")

        if not os.path.exists(detection_dir):
            os.makedirs(detection_dir)

        self.grid_mask=self.export_grid(detection_dir,self.bf_img, params, self.fl_img, show_grid=self.showPlot.isChecked())
        self.spinner_widget.stop()
        self.statusBar().showMessage("Grid detection finished", 2000)
        self.repaint()


    def start_processing(self):
        """
        Start the image processing based on the selected processing function and configuration.

        This method performs image processing using the selected processing function. The results are saved in the
        "processing" directory within the result folder.

        Args:
            None

        Returns:
            None
        """

        processing_fcn_name=self.process_comboBox.currentText()
        if self.result_dir is None:
            self.statusBar().showMessage("Select image first",5000)
            return

        if processing_fcn_name == "None":
            self.statusBar().showMessage("Select processing function first",5000)
            return
        
        if processing_fcn_name == 'color_evaluation' and not hasattr(self, 'method_selection'):
            self.statusBar().showMessage("Select color first",5000)
            return

        self.statusBar().showMessage("Processing...")
        self.spinner_widget.start()
        self.repaint()
        
        save = self.saveProcessing.isChecked()

        if self.fl_img is None:
            processing_dir = os.path.join(self.result_dir, "processing")
        else:
            ind = self.fl_lbl.text().find('.')
            processing_dir = os.path.join(self.result_dir, f"processing_{self.fl_lbl.text()[:ind]}")

        if not os.path.exists(processing_dir):
            os.makedirs(processing_dir)

        src = os.path.join(self.result_dir, "grid.params")

        if not os.path.isfile(src):
                self.spinner_widget.stop()  
                self.statusBar().showMessage("No parameter file found. Evaluate grid first!")
                return

        if processing_fcn_name=="mean_intensity_fluorescence" or processing_fcn_name=="cells_per_spot":
            if self.fl_img is None:
                self.spinner_widget.stop()
                self.statusBar().showMessage("No fluorescent picture chosen...")
                return

        params = pickle.load(open(src,"rb"))

        args_key = self.cfg["processing_fcn"]["selected"]
        args = self.cfg["processing_fcn"][args_key]

        processing_fcn = get_proccesing_fcn(processing_fcn_name,args)
        

        #---------------------------------------------------------------------------------------------------------------------------------------------
        #---------------------------------------------------------------------------------------------------------------------------------------------

        if processing_fcn_name == 'color_evaluation':
            if self.method_selection.method == 'RGB':
                results_1, results_2 = processing_fcn.run_process(self.bf_img,params, self.fl_img, self.method_selection.c1, 
                                                                            self.method_selection.c2, self.method_selection.base_color)
                if self.saveProcessing.isChecked():
                    processing_fcn.export_results(processing_dir, results_1, self.bf_img,params,self.fl_img, results_2, self.method_selection.c1, self.method_selection.c2)
            else:
                results = processing_fcn.run_process(self.bf_img,params, self.fl_img, channel=self.method_selection.hsv_channel)
                if self.saveProcessing.isChecked():
                    processing_fcn.export_results(processing_dir, results, self.bf_img,params,self.fl_img)
        elif processing_fcn_name == 'build_annotated_dataset':
            try:
                px_win_size, split_ratio = self.define_annotation_options()
                results= processing_fcn.run_process(self.bf_img,params,px_win_size,split_ratio)
                if len(results[0])<=1 or len(results[1])<=1:
                    QtWidgets.QMessageBox.warning(self, 'Warning',"The chosen split ratio leads to a test or train dataset with no elements. Please confirm!")
                if self.saveProcessing.isChecked():
                    processing_fcn.export_results(processing_dir, results, px_win_size, self.bf_img,params)
            except UnboundLocalError:
               self.spinner_widget.stop()
               self.statusBar().showMessage("Invalid split ratio. Please check input!")
               return
            except ValueError:
               self.spinner_widget.stop()
               self.statusBar().showMessage("Invalid window size. Please check input!")
               return
        else:
            results = processing_fcn.run_process(self.bf_img,params, self.fl_img)
            if self.saveProcessing.isChecked():
                processing_fcn.export_results(processing_dir, results, self.bf_img,params,self.fl_img)

        #---------------------------------------------------------------------------------------------------------------------------------------------
        #---------------------------------------------------------------------------------------------------------------------------------------------

        self.spinner_widget.stop()
        self.statusBar().showMessage("Processing finished", 2000)
        self.repaint()

    def define_annotation_options(self):
        """
        Define the options for image annotation, including window size and split ratio.

        Args:
            None

        Returns:
            Tuple[int, float]: A tuple containing the pixel window size and the split ratio.
        """
        height, width=self.bf_img.shape[:2]
        min_f=min(height, width)
        num1, ok1= QtWidgets.QInputDialog.getInt(self,"Define Window Size","Enter the amount of pixels the crop should be sized with")
        if ok1 and num1<min_f and num1>0:
            px_win_size=num1
        else:
            return 0,0
        num2,ok2 = QtWidgets.QInputDialog.getInt(self,"Define Split Ratio","Enter the proportion of data, that should be used for testing (0-100 %)")
        if ok2 and num2<100 and num2>0:
            split_ratio=float(num2/100)
        else:
            pass
        return px_win_size, split_ratio

    def color_selection(self):
        """
        Open a color selection dialog for processing.

        Args:
            None

        Returns:
            None
        """

        self.method_selection = MethodSelection()
        self.method_selection.show()
    
    def yaml2dict(self,yaml_path: str):
        """
        Convert a YAML file to a dictionary.

        Args:
            yaml_path (str): The path to the YAML file.

        Returns:
            dict: The dictionary parsed from the YAML file.
        """

        cfg_yaml = yaml.load(open(yaml_path), Loader=yaml.FullLoader)
        return ConfigParser.read(cfg_yaml)

    def get_args(self,selection_str):
        key = "processing_fcn"
        if self.result_dir is None:
            item = self.cfg[key]["selected"]
            idx= self.process_comboBox.findText(item, QtCore.Qt.MatchFixedString)
            self.process_comboBox.blockSignals(True)
            self.process_comboBox.setCurrentIndex(idx)
            self.process_comboBox.blockSignals(False)
            self.statusBar().showMessage("Select image first",5000)
            return
        src = os.path.join(self.result_dir, "cfg.json")

        cfg_path = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))),  "config")

        if os.path.isfile(src):
            with open(src, 'r') as f:
                self.cfg = json.load(f)

        try:
            cfg_path = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))),  "config")
            default_args = self.yaml2dict(os.path.join(cfg_path, f"{key}.yaml"))[str(selection_str)]
            default_keys = list(default_args.keys())
            if str(selection_str) not in self.cfg[key]:
                args=copy.copy(default_args)
                for default_key in default_keys:
                    args[default_key]=None
            else:
                args=self.cfg[key][str(selection_str)]
                args_keys = list(args.keys())
                missing_keys = set(default_keys) - set(args_keys)
                for missing_key in missing_keys:
                    args[missing_key]=None
            
            if len(args)>0 and not self.initiation:
                parameter_tbl = ParameterTable(default_args,args)
                result=parameter_tbl.exec_()
                if result==QtWidgets.QDialog.Accepted:
                    self.cfg[key][str(selection_str)]=parameter_tbl.get_custom_parameter()
                else:
                    item = self.cfg[key]["selected"]
                    idx= self.process_comboBox.findText(item, QtCore.Qt.MatchFixedString)
                    self.process_comboBox.blockSignals(True)
                    self.process_comboBox.setCurrentIndex(idx)
                    self.process_comboBox.blockSignals(False)
                    return False
            elif len(args)>0 and self.initiation:
                self.cfg[key][str(selection_str)]=args
            else:
                self.cfg[key][str(selection_str)] = dict()

        except:
            self.cfg[key][str(selection_str)] = dict()

        self.cfg[key]["selected"]=str(selection_str)

        with open(os.path.join(self.result_dir, "cfg.json"), 'w') as f:
            json.dump(self.cfg, f)

        return True

    def change_visibility(self):
        detector = self.detector_comboBox.currentText()
        if detector == "Automated":
            self.scanner_group.setVisible(True)
            self.manuel_grid_group.setVisible(False)
        elif detector == "Semi-Automated":
            self.manuel_grid_group.setVisible(True)
            self.scanner_group.setVisible(False)

    def update_table(self):
        """
        Update the grid details table based on the number of grid groups.

        Args:
            None

        Returns:
            None
        """
            
        self.num_groups = int(self.num_grid_groups_lineedit.text())
        self.grid_details_tbl.setRowCount(3)
        self.grid_details_tbl.setColumnCount(self.num_groups)
        self.grid_details_tbl.setVerticalHeaderLabels(["Grid Group","Rows","Columns"])
        for i in range(0,self.num_groups):
            self.grid_details_tbl.setItem(0,i,QtWidgets.QTableWidgetItem(str(i)))

    def get_grid_details(self):
        """
        Get grid details (rows and columns) from the grid details table.

        Args:
            None

        Returns:
            List[Tuple[int, int]]: A list of tuples, where each tuple contains the number of rows and columns for a grid group.
            Returns None if the input is invalid.
        """

        grid_shape=list()
        for i in range(0,self.num_groups):
            rows_text=self.grid_details_tbl.item(1,i).text()
            columns_text=self.grid_details_tbl.item(2,i).text()
            
            if rows_text.isdigit() and columns_text.isdigit():
                rows = int(rows_text)
                columns = int(columns_text)
                grid_shape.append((rows, columns))
            else:
                grid_shape=None

        return grid_shape

    def export_grid(self,detection_dir,bf_img,params, fl_img=None, show_grid=True, ):
        """
        Export the grid detection results, optionally displaying the grid.

        Args:
            detection_dir (str): The directory where grid detection results will be saved.
            bf_img (numpy.ndarray): The brightfield image.
            params (GridParameter): The grid detection parameters.
            fl_img (numpy.ndarray, optional): The fluorescent image. Defaults to None.
            show_grid (bool, optional): Whether to display the grid during export. Defaults to True.

        Returns:
            numpy.ndarray: The grid mask.
        """

        angle=params.get_angle()
        d = params.get_diameter()
        grid = params.get_grid()
        shape = params.get_shape()

        try:
            bf_img=ManualDetector.rotate(self,image=bf_img,angle=angle).astype("int")
        except cv2.error:
            bf_img=rotate(bf_img,angle,resize=True, preserve_range=True, order=0).astype("int")

        h,w=bf_img.shape[0:2]
        if w > h:
            h=h/w
            w=1
        elif h>w:
            w=w/h
            h=1
        else:
            w=1
            h=1

        if fl_img is not None:
            try:
                fl_img=ManualDetector.rotate(self,image=fl_img,angle=angle).astype("int")
            except cv2.error:
                fl_img=rotate(fl_img,angle,resize=True, preserve_range=True, order=0).astype("int")
            
            detection_fl_dir = os.path.join(
                os.path.split(detection_dir)[0], 
                "detection_fl")
            if not os.path.exists(detection_fl_dir):
                os.makedirs(detection_fl_dir)
            estimatedgriddialog = EstimatedGridDialog(angle,d,grid,shape,bf_img, detection_dir, self.result_dir, fl_img, detection_fl_dir)
        else:
            estimatedgriddialog = EstimatedGridDialog(angle,d,grid,shape,bf_img, detection_dir, self.result_dir) 

        if show_grid:
            estimatedgriddialog.exec_()
        mask=estimatedgriddialog.get_mask()
        return mask

