from GS.grid_estimators.grid_scanner import Scanner
from GS.grid_estimators.manual_detector import ManualDetector
from GS.processing.crop_spots import CropSpots
from GS.processing.cells_per_spot import CellsPerSpot
from GS.processing.cells_per_spot_dl import CellsPerSpotDL
from GS.processing.mean_intensity_fl import MeanIntensityFluorescence
from GS.processing.spheroid_analysis import SpheroidAnalysis
from GS.processing.color_evaluation import ColorEvaluation
from GS.processing.build_annotated_dataset import BuildAnnotatedDataset

#from GS.dnn.model.zoo.compositions.unet_semantic import UnetSemantic
from DLIP.models.zoo.compositions.unet_semantic import UnetSemantic

KNOWN_GRID_DETECTORS ={
    "Semi-Automated": ManualDetector,
    "Automated": Scanner
}

KNOWN_PROCESSING_FCN ={
    "crop_spots": CropSpots,
    "cells_per_spot": CellsPerSpot,
    "mean_intensity_fluorescence" : MeanIntensityFluorescence,
    "spheroid_analysis": SpheroidAnalysis,
    "color_evaluation": ColorEvaluation,
    "cells_per_spot_dl": CellsPerSpotDL,
    "build_annotated_dataset": BuildAnnotatedDataset
}

KNOWN_MODELS = {
    "unetsemantic": UnetSemantic,
}

def get_estimator(estimator_name: str):
    return KNOWN_GRID_DETECTORS[estimator_name]()

def get_known_detectors():
    return KNOWN_GRID_DETECTORS

def get_proccesing_fcn(processing_fcn_name:str,args):
    return KNOWN_PROCESSING_FCN[processing_fcn_name](**args)

def get_known_processing_fcns():
    return KNOWN_PROCESSING_FCN

def get_model(model_name: str):
    return KNOWN_MODELS[model_name]

#__all__={"KNOWN_GRID_DETECTORS","KNOWN_MODELS","KNOWN_PROCESSING_FCN","get_estimator","get_known_detectors","get_known_processing_fcns","get_model","get_proccesing_fcn"}