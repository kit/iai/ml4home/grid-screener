import os

import cv2
import numpy as np
import tifffile


class Image:
    def __init__(self, file_path: str):
        """
        Initialize an instance of the Image class with an image file.

        Args:
            self (object): The instance of the class.
            file_path (str): The path to the image file.

        Note:
            This method initializes an instance of the Image class with the provided image file.
            It attempts to read the image using tifffile and falls back to OpenCV if tifffile fails.
            If the image has a data type of np.uint16, it is converted to np.uint8 with proper scaling.
        """
        
        self.file_path = file_path
        try:
            self.img = tifffile.imread(self.file_path)
        except tifffile.tifffile.TiffFileError:
            self.img = cv2.imread(self.file_path,-1)


        if self.img.dtype == np.uint16:
            self.img = (self.img.astype(np.float16) / 65535 * 255).astype(np.uint8)

    def numpy(self) -> np.ndarray:
        return self.img

    def get_directory(self):
        return os.path.split(self.file_path)[0]

    def get_name(self) -> str:
        file_name = os.path.basename(self.file_path)
        return file_name.replace(".{}".format(self.get_extension()), "")

    def get_extension(self) -> str:
        return os.path.basename(self.file_path).split(".")[-1]

    def rotate(self):
        self.img = cv2.rotate(self.img, cv2.ROTATE_90_COUNTERCLOCKWISE)
        return self

    def rotate_if_portrait(self):
        """
        Rotate the image if it is in portrait orientation (height > width).

        Returns:
            Image: The rotated image if in portrait orientation, or the original image.

        Raises:
            ValueError: If the image has an unsupported number of channels.
        """

        if len(self.img.shape) == 3:
            h, w, _ = self.img.shape
        elif len(self.img.shape) == 2:
            h, w = self.img.shape
        else:
            raise ValueError("The given Image has to be RGB (3 channels) or Grayscale (2 channels)! "
                             f"Got {len(self.img.shape)} channels")
        if h / w > 1.5:
            return self.rotate()
        return self

    def resolution(self):
        tif_meta_data = tifffile.TiffFile(self.file_path)
        return tif_meta_data.pages[0].tags["XResolution"].value[0] / tif_meta_data.pages[0].tags["XResolution"].value[1]
