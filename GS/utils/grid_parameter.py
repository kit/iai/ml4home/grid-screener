from abc import ABC


class GridParameter(ABC):
    def __init__(self, angle, grid, shape, diameter):
        super().__init__()

        self.angle      = angle
        self.grid       = grid
        self.shape      = shape
        self.diameter   = diameter

    def get_angle(self):
        return self.angle
    
    def get_grid(self):
        return self.grid
    
    def get_shape(self):
        return self.shape
    
    def get_diameter(self):
        return self.diameter
    
    