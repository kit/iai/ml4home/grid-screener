import sys
from pathlib import Path

from PyQt5 import QtWidgets

from GS.utils.splash_screen import SplashScreen

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    splash_screen_img = str(Path(__file__).parent.joinpath("gui").joinpath("logo.png"))
    splash_screen = SplashScreen(splash_screen_img)
    splash_screen.show()

    from GS.gui.grid_screener import GridScreenerGui
    gs_gui = GridScreenerGui()
    
    splash_screen.close()

    gs_gui.show()
    sys.exit(app.exec_())
